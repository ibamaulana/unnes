-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Waktu pembuatan: 03 Jun 2018 pada 16.30
-- Versi server: 5.6.38
-- Versi PHP: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_kkn_unnes`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `agenda`
--

CREATE TABLE `agenda` (
  `id_agenda` int(11) NOT NULL,
  `nama_agenda` varchar(225) NOT NULL,
  `deskripsi` varchar(225) NOT NULL,
  `tanggal` varchar(225) NOT NULL,
  `lokasi` varchar(225) NOT NULL,
  `tujuan` varchar(225) NOT NULL,
  `lingkup` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `agenda`
--

INSERT INTO `agenda` (`id_agenda`, `nama_agenda`, `deskripsi`, `tanggal`, `lokasi`, `tujuan`, `lingkup`) VALUES
(8, 'Rencana penanaman pohon di wilayah genuk', 'Penanaman pohon mangroove di dekat sungai genuk pada hari sabtu sore.', '05/27/2018', 'Genuk, Semarang', 'Upaya pengurangan rob di daerah genuk semarang.', 'Reboisasi, Penanaman pohon, GO green'),
(9, 'Pembuatan Kolam ikan', 'Membuat kolam ikan dengan warga pada hari jumat ', '05/29/2018', 'Desa Banyumanik, Semarang', 'Menambah ksejahteraan desa banyumanik dengan pengadaan kolam', 'Perikanan desa'),
(10, 'Pembuatan Lapangan Tembak', 'Membuat kolam ikan dengan warga pada hari jumat ', '05/29/2018', 'Desa Banyumanik, Semarang', 'Menambah ksejahteraan desa banyumanik dengan pengadaan kolam', 'Lapangan Tembak'),
(11, 'Pembuatan Lapangan Sepakbola', 'Membuat kolam ikan dengan warga pada hari jumat ', '05/29/2018', 'Desa Banyumanik, Semarang', 'Menambah ksejahteraan desa banyumanik dengan pengadaan kolam', 'Lapangan Tembak'),
(12, 'Pembuatan Balai Desa', 'Membuat kolam ikan dengan warga pada hari jumat ', '05/29/2018', 'Desa Banyumanik, Semarang', 'Menambah ksejahteraan desa banyumanik dengan pengadaan kolam', 'Lapangan Tembak');

-- --------------------------------------------------------

--
-- Struktur dari tabel `berita`
--

CREATE TABLE `berita` (
  `id_berita` int(11) NOT NULL,
  `nama_berita` varchar(225) NOT NULL,
  `deskripsi` varchar(225) NOT NULL,
  `tanggal` varchar(225) NOT NULL,
  `penulis` varchar(225) NOT NULL,
  `lingkup` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `berita`
--

INSERT INTO `berita` (`id_berita`, `nama_berita`, `deskripsi`, `tanggal`, `penulis`, `lingkup`) VALUES
(2, 'Berita satu', 'berita tentang bagaimana terjadinya sebuah kecelakaan di dekat terminal banyumanik.\r\n', '05/27/2018', 'SCTV', 'Lingkup pendidikan'),
(3, 'Unnes menjadi kampus terbaik di semarang tahun 2018', 'Pada tahun 2018 ini , telah dipilih kampus terbaik di semarang adalah UNNES\r\n', '05/28/2018', 'google api ', 'Pendidikan'),
(4, 'Unnes menuju kampus terbaik di semarang tahun 2018', 'Pada tahun 2018 ini , telah dipilih kampus terbaik di semarang adalah UNNES\r\n', '05/28/2018', 'google api ', 'Pendidikan'),
(5, 'Unnes memenangkan piala internasional', 'Pada tahun 2018 ini , telah dipilih kampus terbaik di semarang adalah UNNES\r\n', '05/28/2018', 'google api ', 'Pendidikan'),
(6, 'Kegiatan mahasiswa UNNES disorot oleh pak jokowi', 'Pada tahun 2018 ini , telah dipilih kampus terbaik di semarang adalah UNNES\r\n', '05/28/2018', 'google api ', 'Pendidikan');

-- --------------------------------------------------------

--
-- Struktur dari tabel `gambar_desa`
--

CREATE TABLE `gambar_desa` (
  `id_gambar_desa` int(11) NOT NULL,
  `id_desa` int(11) NOT NULL,
  `path` varchar(225) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `gambar_desa`
--

INSERT INTO `gambar_desa` (`id_gambar_desa`, `id_desa`, `path`, `status`) VALUES
(71, 22, 'assets/desa/desa-280518102431-1.jpg', 1),
(72, 22, 'assets/desa/desa-280518102431-2.jpg', 1),
(73, 27, 'assets/desa/desa-280518102524-1.jpg', 1),
(74, 27, 'assets/desa/desa-280518102524-2.jpg', 1),
(75, 27, 'assets/desa/desa-280518102538-1.jpg', 1),
(76, 26, 'assets/desa/desa-280518102543-1.jpg', 1),
(77, 26, 'assets/desa/desa-280518102556-1.jpg', 1),
(81, 23, 'assets/desa/desa-030618205709-1.jpg', 1),
(83, 23, 'assets/desa/desa-030618205751-1.jpg', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `gambar_kegiatan`
--

CREATE TABLE `gambar_kegiatan` (
  `id_gambar_kegiatan` int(11) NOT NULL,
  `id_kegiatan` int(11) NOT NULL,
  `path` varchar(225) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `gambar_kegiatan`
--

INSERT INTO `gambar_kegiatan` (`id_gambar_kegiatan`, `id_kegiatan`, `path`, `status`) VALUES
(9, 13, 'assets/kegiatan/kegiatan-280518103338-1.jpeg', 1),
(10, 13, 'assets/kegiatan/kegiatan-280518103338-2.jpeg', 1),
(11, 13, 'assets/kegiatan/kegiatan-280518103338-3.jpeg', 1),
(12, 14, 'assets/kegiatan/kegiatan-280518103343-1.jpeg', 1),
(13, 14, 'assets/kegiatan/kegiatan-280518103343-2.jpeg', 1),
(14, 14, 'assets/kegiatan/kegiatan-280518103343-3.jpeg', 1),
(15, 15, 'assets/kegiatan/kegiatan-280518103347-1.jpeg', 1),
(16, 15, 'assets/kegiatan/kegiatan-280518103347-2.jpeg', 1),
(17, 15, 'assets/kegiatan/kegiatan-280518103347-3.jpeg', 1),
(18, 16, 'assets/kegiatan/kegiatan-280518103351-1.jpeg', 1),
(19, 16, 'assets/kegiatan/kegiatan-280518103351-2.jpeg', 1),
(20, 16, 'assets/kegiatan/kegiatan-280518103351-3.jpeg', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `kegiatan`
--

CREATE TABLE `kegiatan` (
  `id_kegiatan` int(11) NOT NULL,
  `id_desa` int(11) NOT NULL,
  `nama_kegiatan` varchar(225) NOT NULL,
  `deskripsi` varchar(225) NOT NULL,
  `tanggal` varchar(225) NOT NULL,
  `bidang` int(11) NOT NULL,
  `hasil` varchar(225) NOT NULL,
  `jenis` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `kegiatan`
--

INSERT INTO `kegiatan` (`id_kegiatan`, `id_desa`, `nama_kegiatan`, `deskripsi`, `tanggal`, `bidang`, `hasil`, `jenis`) VALUES
(13, 20, 'Kegiatan Desa Sampangan', 'Pembuatan kebon desa', '05/28/2018', 1, 'kebon desa', 0),
(14, 20, 'Kegiatan Desa Sampangan 2', 'Pembuatan kebon desa 2', '05/28/2018', 3, 'kebon desa 2', 0),
(15, 19, 'Kegiatan Desa Banyumanik', 'Pembuatan kebon desa ', '05/28/2018', 3, 'kebon desa ', 0),
(16, 21, 'Kegiatan Desa Banyumanik', 'Pembuatan kebon desa ', '05/28/2018', 3, 'kebon desa ', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `profil`
--

CREATE TABLE `profil` (
  `id_profil` int(11) NOT NULL,
  `namaweb` varchar(225) NOT NULL,
  `sloganweb` varchar(225) NOT NULL,
  `deskripsiweb` varchar(225) NOT NULL,
  `kesanpesanweb` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `profil`
--

INSERT INTO `profil` (`id_profil`, `namaweb`, `sloganweb`, `deskripsiweb`, `kesanpesanweb`) VALUES
(1, 'KKN SANGGAM', 'KKN UNNES TERBAIK', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo con', 'Morbi a suscipit ipsum. Suspendisse mollis libero ante. Pellentesque finibus convallis nulla vel placerat. Nulla ipsum dolor sit amet, consectetur adipiscing elitut eleifend nisl. Ullam elementum tincidunt massa, a pulvinar l');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_desa`
--

CREATE TABLE `tb_desa` (
  `id_desa` int(11) NOT NULL,
  `nama_desa` varchar(225) NOT NULL,
  `alamat_desa` varchar(225) NOT NULL,
  `lokasi_desa` varchar(225) NOT NULL,
  `potensi_desa` varchar(225) NOT NULL,
  `sarana` varchar(225) NOT NULL,
  `jenis` int(11) NOT NULL,
  `deskripsi` varchar(225) NOT NULL,
  `deleted` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `tb_desa`
--

INSERT INTO `tb_desa` (`id_desa`, `nama_desa`, `alamat_desa`, `lokasi_desa`, `potensi_desa`, `sarana`, `jenis`, `deskripsi`, `deleted`) VALUES
(19, 'Desa Banyumanik', 'Jl. Banyumanik no. 123', 'Semarang', 'Perairan, Sungai, Kebun', 'POS Ronda, Puskesmas', 1, 'Desa yang memiliki kekayaan alam yang berlimpah. desa ini merupakan dewa wisata pada kota semarang. banyak terdapat potensi dna lain lain yang masih belum ter eksplor oleh penduduk semarang.', 0),
(20, 'Desa Sampangan', 'Jl. Sampangan no. 123', 'Semarang', 'Perairan, Sungai, Kebun', 'POS Ronda, Puskesmas', 1, 'Desa yang memiliki kekayaan alam yang berlimpah. desa ini merupakan dewa wisata pada kota semarang. banyak terdapat potensi dna lain lain yang masih belum ter eksplor oleh penduduk semarang.', 0),
(21, 'Desa Tlogosari', 'Jl. Tlogosari no. 123', 'Semarang', 'Perairan, Sungai, Kebun', 'POS Ronda, Puskesmas', 1, 'Desa yang memiliki kekayaan alam yang berlimpah. desa ini merupakan dewa wisata pada kota semarang. banyak terdapat potensi dna lain lain yang masih belum ter eksplor oleh penduduk semarang.', 0),
(22, 'Desa Tembalang', 'Jl. Tembalang no. 123', 'Semarang', 'Perairan, Sungai, Kebun', 'POS Ronda, Puskesmas', 1, 'Desa yang memiliki kekayaan alam yang berlimpah. desa ini merupakan dewa wisata pada kota semarang. banyak terdapat potensi dna lain lain yang masih belum ter eksplor oleh penduduk semarang.', 0),
(23, 'Desa Ngesrep', 'Jl. Ngesrep no. 123', 'Semarang', 'Perairan, Sungai, Kebun', 'POS Ronda, Puskesmas', 1, 'Desa yang memiliki kekayaan alam yang berlimpah. desa ini merupakan dewa wisata pada kota semarang. banyak terdapat potensi dna lain lain yang masih belum ter eksplor oleh penduduk semarang.', 0),
(24, 'Desa Pleburan', 'Jl. Pleburan no. 123', 'Semarang', 'Perairan, Sungai, Kebun', 'POS Ronda, Puskesmas', 1, 'Desa yang memiliki kekayaan alam yang berlimpah. desa ini merupakan dewa wisata pada kota semarang. banyak terdapat potensi dna lain lain yang masih belum ter eksplor oleh penduduk semarang.', 1),
(25, 'Desa Pleburan', 'Jl. Pleburan no. 123', 'Semarang', 'Perairan, Sungai, Kebun', 'POS Ronda, Puskesmas', 2, 'Desa yang memiliki kekayaan alam yang berlimpah. desa ini merupakan dewa wisata pada kota semarang. banyak terdapat potensi dna lain lain yang masih belum ter eksplor oleh penduduk semarang.', 0),
(26, 'Desa Singosari', 'Jl. Singosari no. 123', 'Semarang', 'Perairan, Sungai, Kebun', 'POS Ronda, Puskesmas', 2, 'Desa yang memiliki kekayaan alam yang berlimpah. desa ini merupakan dewa wisata pada kota semarang. banyak terdapat potensi dna lain lain yang masih belum ter eksplor oleh penduduk semarang.', 0),
(27, 'Desa Pandanaran', 'Jl. Pandanaran no. 123', 'Semarang', 'Perairan, Sungai, Kebun', 'POS Ronda, Puskesmas', 2, 'Desa yang memiliki kekayaan alam yang berlimpah. desa ini merupakan dewa wisata pada kota semarang. banyak terdapat potensi dna lain lain yang masih belum ter eksplor oleh penduduk semarang.', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_user`
--

CREATE TABLE `tb_user` (
  `id_user` int(11) NOT NULL,
  `nama` varchar(225) NOT NULL,
  `username` varchar(225) NOT NULL,
  `password` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `tb_user`
--

INSERT INTO `tb_user` (`id_user`, `nama`, `username`, `password`) VALUES
(1, 'admin', 'admin', '21232f297a57a5a743894a0e4a801fc3');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `agenda`
--
ALTER TABLE `agenda`
  ADD PRIMARY KEY (`id_agenda`);

--
-- Indeks untuk tabel `berita`
--
ALTER TABLE `berita`
  ADD PRIMARY KEY (`id_berita`);

--
-- Indeks untuk tabel `gambar_desa`
--
ALTER TABLE `gambar_desa`
  ADD PRIMARY KEY (`id_gambar_desa`);

--
-- Indeks untuk tabel `gambar_kegiatan`
--
ALTER TABLE `gambar_kegiatan`
  ADD PRIMARY KEY (`id_gambar_kegiatan`);

--
-- Indeks untuk tabel `kegiatan`
--
ALTER TABLE `kegiatan`
  ADD PRIMARY KEY (`id_kegiatan`);

--
-- Indeks untuk tabel `profil`
--
ALTER TABLE `profil`
  ADD PRIMARY KEY (`id_profil`);

--
-- Indeks untuk tabel `tb_desa`
--
ALTER TABLE `tb_desa`
  ADD PRIMARY KEY (`id_desa`);

--
-- Indeks untuk tabel `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `agenda`
--
ALTER TABLE `agenda`
  MODIFY `id_agenda` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT untuk tabel `berita`
--
ALTER TABLE `berita`
  MODIFY `id_berita` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `gambar_desa`
--
ALTER TABLE `gambar_desa`
  MODIFY `id_gambar_desa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=84;

--
-- AUTO_INCREMENT untuk tabel `gambar_kegiatan`
--
ALTER TABLE `gambar_kegiatan`
  MODIFY `id_gambar_kegiatan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT untuk tabel `kegiatan`
--
ALTER TABLE `kegiatan`
  MODIFY `id_kegiatan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT untuk tabel `profil`
--
ALTER TABLE `profil`
  MODIFY `id_profil` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `tb_desa`
--
ALTER TABLE `tb_desa`
  MODIFY `id_desa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT untuk tabel `tb_user`
--
ALTER TABLE `tb_user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
