<!DOCTYPE html>
<html lang="en">

<?php $this->load->view('head') ?>

<body>
  <main>
    <!-- Header -->
    <?php $this->load->view('header') ?>
    <!-- End Header -->

    <!-- Breadcrumbs -->
    <section class="g-bg-gray-light-v5 g-py-50">
      <div class="container">
        <div class="d-sm-flex text-center">
          <div class="align-self-center">
            <h2 class="h3 g-font-weight-300 w-100 g-mb-10 g-mb-0--md"><?= $title ?></h2>
          </div>

          <div class="align-self-center ml-auto">
            <ul class="u-list-inline">
              <li class="list-inline-item g-mr-5">
                <a class="u-link-v5 g-color-main g-color-primary--hover" href="<?= site_url() ?>">Home</a>
                <i class="g-color-gray-light-v2 g-ml-5">/</i>
              </li>
              <li class="list-inline-item g-mr-5">
                <a class="u-link-v5 g-color-main g-color-primary--hover" href="<?= site_url('berita') ?>">Berita</a>
                <i class="g-color-gray-light-v2 g-ml-5">/</i>
              </li>
              <li class="list-inline-item g-color-primary">
                <span>Detail Berita</span>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </section>
    <!-- End Breadcrumbs -->

    <!-- Portfolio Single Item -->
    <section class="container g-py-100">
      <?php foreach ($berita->result() as $key) {?>
      <div class="row g-mb-70">
        <div class="col-md-8 g-mb-30">
          <div class="mb-5">
            <h2 class="g-color-black mb-1"><?= $key->nama_berita ?></h2>
            <span class="d-block lead mb-4"><i class="fa fa-calendar g-mr-5 g-ml-5"></i><?= date('d M Y', strtotime($key->tanggal)); ?></span>
            <p><?= $key->deskripsi ?></p>
          </div>
        </div>

        <div class="col-md-4 g-mb-30">
          <!-- Client -->
          <div class="mb-5">
            <h3 class="h5 g-color-black mb-3">Sumber Berita:</h3>
            <p class="g-color-gray-dark-v1 g-mb-10"><?= $key->penulis ?></p>
          </div>
          <!-- End Client -->

          <!-- Tags -->
          <div class="g-mb-30">
            <h3 class="h5 g-color-black mb-3">Lingkup Berita:</h3>
            <p class="g-color-gray-dark-v1 g-mb-10"><?= $key->lingkup ?></p>
          </div>
          <!-- End Tags -->
        </div>
      </div>
    <?php }?>
      <!-- Pagination -->
      <nav class="text-center" aria-label="Page Navigation">
        <ul class="list-inline mb-0">
          <li class="list-inline-item mr-0">
            <a class="u-pagination-v1__item g-color-gray-dark-v4 g-font-size-20 g-py-2 g-px-10" href="<?= site_url('berita') ?>" data-toggle="tooltip" data-placement="top" title="View All">
              <i class="icon-grid"></i>
            </a>
          </li>
        </ul>
      </nav>
      <!-- End Pagination -->
    </section>
    <!-- End Portfolio Single Item -->

    <!-- Call To Action -->
    <section class="g-bg-primary g-color-white g-pa-30 g-mt-50" style="background-image: url(../../assets/img/bg/pattern5.png);">
      <div class="d-md-flex justify-content-md-center text-center">
        <div class="align-self-md-center">
          <p class="lead g-font-weight-400 g-mr-20--md g-mb-15 g-mb-0--md">KKN UNNES TERBAIK</p>
        </div>
      </div>
    </section>
    <!-- End Call To Action -->

    <!-- Copyright Footer -->
    <footer class="g-bg-gray-dark-v1 g-color-white-opacity-0_8 g-py-20">
      <div class="container">
        <div class="row">
          <div class="col-md-8 text-center text-md-left g-mb-10 g-mb-0--md">
            <div class="d-lg-flex">
              <small class="d-block g-font-size-default g-mr-10 g-mb-10 g-mb-0--md">2018 &copy; Tim KKN Universitas Negeri Semarang.</small>
            </div>
          </div>

          <div class="col-md-4 align-self-center">
            <ul class="list-inline text-center text-md-right mb-0">
              <li class="list-inline-item g-mx-10" data-toggle="tooltip" data-placement="top" title="Facebook">
                <a href="#!" class="g-color-white-opacity-0_5 g-color-white--hover">
                  <i class="fa fa-facebook"></i>
                </a>
              </li>
              <li class="list-inline-item g-mx-10" data-toggle="tooltip" data-placement="top" title="Skype">
                <a href="#!" class="g-color-white-opacity-0_5 g-color-white--hover">
                  <i class="fa fa-skype"></i>
                </a>
              </li>
              <li class="list-inline-item g-mx-10" data-toggle="tooltip" data-placement="top" title="Linkedin">
                <a href="#!" class="g-color-white-opacity-0_5 g-color-white--hover">
                  <i class="fa fa-linkedin"></i>
                </a>
              </li>
              <li class="list-inline-item g-mx-10" data-toggle="tooltip" data-placement="top" title="Pinterest">
                <a href="#!" class="g-color-white-opacity-0_5 g-color-white--hover">
                  <i class="fa fa-pinterest"></i>
                </a>
              </li>
              <li class="list-inline-item g-mx-10" data-toggle="tooltip" data-placement="top" title="Twitter">
                <a href="#!" class="g-color-white-opacity-0_5 g-color-white--hover">
                  <i class="fa fa-twitter"></i>
                </a>
              </li>
              <li class="list-inline-item g-mx-10" data-toggle="tooltip" data-placement="top" title="Dribbble">
                <a href="#!" class="g-color-white-opacity-0_5 g-color-white--hover">
                  <i class="fa fa-dribbble"></i>
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </footer>
    <!-- End Copyright Footer -->
    <a class="js-go-to u-go-to-v1" href="#!" data-type="fixed" data-position='{
     "bottom": 15,
     "right": 15
   }' data-offset-top="400" data-compensation="#js-header" data-show-effect="zoomIn">
      <i class="hs-icon hs-icon-arrow-top"></i>
    </a>
  </main>

  <div class="u-outer-spaces-helper"></div>


  <?php $this->load->view('scripts') ?>

</body>

</html>