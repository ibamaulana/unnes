<!DOCTYPE html>
<html lang="en">

<?php $this->load->view('head') ?>

<body>
  <main>
    <!-- Header -->
    <?php $this->load->view('header') ?>
    <!-- End Header -->

    <!-- Breadcrumbs -->
    <section class="g-bg-gray-light-v5 g-py-50">
      <div class="container">
        <div class="d-sm-flex text-center">
          <div class="align-self-center">
            <h2 class="h3 g-font-weight-300 w-100 g-mb-10 g-mb-0--md"><?= $title ?></h2>
          </div>

          <div class="align-self-center ml-auto">
            <ul class="u-list-inline">
              <li class="list-inline-item g-mr-5">
                <a class="u-link-v5 g-color-main g-color-primary--hover" href="<?= site_url() ?>">Home</a>
                <i class="g-color-gray-light-v2 g-ml-5">/</i>
              </li>
              <li class="list-inline-item g-mr-5">
                <a class="u-link-v5 g-color-main g-color-primary--hover" href="<?= site_url('kegiatan/kegiatan') ?>">Kegiatan</a>
                <i class="g-color-gray-light-v2 g-ml-5">/</i>
              </li>
              <li class="list-inline-item g-color-primary">
                <span>Detail Kegiatan</span>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </section>
    <!-- End Breadcrumbs -->

    <!-- Portfolio Single Item -->
    <section class="container g-py-100">
      <!-- Cube Portfolio Blocks - Content -->
      <!-- Cube Portfolio Blocks - Content -->
      <div class="cbp g-mb-50" data-controls="#filterControls1" data-animation="quicksand" data-x-gap="30" data-y-gap="30" data-media-queries='[{"width": 1500, "cols": 3}, {"width": 1100, "cols": 3}, {"width": 800, "cols": 3}, {"width": 480, "cols": 2}, {"width": 300, "cols": 1}]'>
        <!-- Cube Portfolio Blocks - Item -->
        <?php foreach ($gambar->result() as $key) {?>
        <div class="cbp-item identity design">
          <div class="u-block-hover g-parent">
            <img class="img-fluid g-transform-scale-1_1--parent-hover g-transition-0_5 g-transition--ease-in-out" src="<?= site_url().$key->path ?>" alt="Image description">
            <div class="d-flex w-100 h-100 g-bg-black-opacity-0_6 opacity-0 g-opacity-1--parent-hover g-pos-abs g-top-0 g-left-0 g-transition-0_3 g-transition--ease-in u-block-hover__additional--fade u-block-hover__additional--fade-in g-pa-20">
              <div class="align-items-end flex-column mt-auto ml-auto">
                <a class="cbp-lightbox u-icon-v2 u-icon-size--sm g-brd-white g-color-black g-bg-white rounded-circle" href="<?= site_url().$key->path ?>">
                  <i class="icon-communication-017 u-line-icon-pro"></i>
                </a>
              </div>
            </div>
          </div>
        </div>
      <?php }?>
        <!-- End Cube Portfolio Blocks - Item -->
      </div>
      <!-- End Cube Portfolio Blocks - Content -->
      <!-- End Cube Portfolio Blocks - Content -->
      <?php foreach ($kegiatan->result() as $val) {
        if ($val->bidang == 1) {
                    $bidang = 'Ekonomi';
                }else if($val->bidang == 2){
                    $bidang = 'Pendidikan';
                }else if($val->bidang == 3){
                    $bidang = 'Kesehatan';
                }else if($val->bidang == 4){
                    $bidang = 'Lingkungan';
                }else if($val->bidang == 5){
                    $bidang = 'Tidak ada bidang';
                }?>
        <div class="row g-mb-30">
          <div class="col-md-8 g-mb-30">
            <div class="mb-5">
              <h2 class="g-color-black mb-1"><?= $val->nama_kegiatan ?></h2>
              <span class="d-block lead mb-4"><i class="fa fa-map-marker g-mr-5"></i> <?= $val->nama_desa ?> <i class="fa fa-calendar g-mr-5 g-ml-5"></i> <?= date('d-M-Y',strtotime($val->tanggal)); ?></span>
              <p><?= $val->deskripsi ?></p>
            </div>
          </div>

          <div class="col-md-4 g-mb-30">

            <div class="mb-5">
              <h3 class="h5 g-color-black mb-3">Hasil:</h3>
              <p class="g-color-gray-dark-v1 g-mb-10"><?= $val->hasil ?></p>
            </div>

            <div class="mb-5">
              <h3 class="h5 g-color-black mb-3">Bidang:</h3>
              <p class="g-color-gray-dark-v1 g-mb-10"><?= $bidang ?></p>
            </div>
            <!-- End Share -->
          </div>
        </div>
      <?php } ?>
      <!-- Pagination -->
      <nav class="text-center" aria-label="Page Navigation">
        <ul class="list-inline mb-0">
          <li class="list-inline-item mr-0">
            <a class="u-pagination-v1__item g-color-gray-dark-v4 g-font-size-20 g-py-2 g-px-10" href="<?= site_url('kegiatan/kegiatan') ?>" data-toggle="tooltip" data-placement="top" title="View All">
              <i class="icon-grid"></i>
            </a>
          </li>
        </ul>
      </nav>
      <!-- End Pagination -->
    </section>
    <!-- End Portfolio Single Item -->


    <!-- Call To Action -->
    <section class="g-bg-primary g-color-white g-pa-30 g-mt-20" style="background-image: url(../../assets/img/bg/pattern5.png);">
      <div class="d-md-flex justify-content-md-center text-center">
        <div class="align-self-md-center">
          <p class="lead g-font-weight-400 g-mr-20--md g-mb-15 g-mb-0--md">KKN UNNES TERBAIK</p>
        </div>
      </div>
    </section>
    <!-- End Call To Action -->

    <!-- Copyright Footer -->
    <footer class="g-bg-gray-dark-v1 g-color-white-opacity-0_8 g-py-20">
      <div class="container">
        <div class="row">
          <div class="col-md-8 text-center text-md-left g-mb-10 g-mb-0--md">
            <div class="d-lg-flex">
              <small class="d-block g-font-size-default g-mr-10 g-mb-10 g-mb-0--md">2018 &copy; Tim KKN Universitas Negeri Semarang.</small>
            </div>
          </div>

          <div class="col-md-4 align-self-center">
            <ul class="list-inline text-center text-md-right mb-0">
              <li class="list-inline-item g-mx-10" data-toggle="tooltip" data-placement="top" title="Facebook">
                <a href="#!" class="g-color-white-opacity-0_5 g-color-white--hover">
                  <i class="fa fa-facebook"></i>
                </a>
              </li>
              <li class="list-inline-item g-mx-10" data-toggle="tooltip" data-placement="top" title="Skype">
                <a href="#!" class="g-color-white-opacity-0_5 g-color-white--hover">
                  <i class="fa fa-skype"></i>
                </a>
              </li>
              <li class="list-inline-item g-mx-10" data-toggle="tooltip" data-placement="top" title="Linkedin">
                <a href="#!" class="g-color-white-opacity-0_5 g-color-white--hover">
                  <i class="fa fa-linkedin"></i>
                </a>
              </li>
              <li class="list-inline-item g-mx-10" data-toggle="tooltip" data-placement="top" title="Pinterest">
                <a href="#!" class="g-color-white-opacity-0_5 g-color-white--hover">
                  <i class="fa fa-pinterest"></i>
                </a>
              </li>
              <li class="list-inline-item g-mx-10" data-toggle="tooltip" data-placement="top" title="Twitter">
                <a href="#!" class="g-color-white-opacity-0_5 g-color-white--hover">
                  <i class="fa fa-twitter"></i>
                </a>
              </li>
              <li class="list-inline-item g-mx-10" data-toggle="tooltip" data-placement="top" title="Dribbble">
                <a href="#!" class="g-color-white-opacity-0_5 g-color-white--hover">
                  <i class="fa fa-dribbble"></i>
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </footer>
    <!-- End Copyright Footer -->
    <a class="js-go-to u-go-to-v1" href="#!" data-type="fixed" data-position='{
     "bottom": 15,
     "right": 15
   }' data-offset-top="400" data-compensation="#js-header" data-show-effect="zoomIn">
      <i class="hs-icon hs-icon-arrow-top"></i>
    </a>
  </main>

  <div class="u-outer-spaces-helper"></div>


  <?php $this->load->view('scripts') ?>
  <script>

      $(window).on('load', function () {

        // initialization of cubeportfolio
        $.HSCore.components.HSCubeportfolio.init('.cbp');
      });
  </script>

</body>

</html>