<!-- JS Global Compulsory -->
<script src="<?= site_url('template/assets/vendor/jquery/jquery.min.js') ?>"></script>
<script src="<?= site_url('template/assets/vendor/jquery-migrate/jquery-migrate.min.js') ?>"></script>
<script src="<?= site_url('template/assets/vendor/popper.min.js') ?>"></script>
<script src="<?= site_url('template/assets/vendor/bootstrap/bootstrap.min.js') ?>"></script>

<!-- JS Implementing Plugins -->
<script src="<?= site_url('template/assets/vendor/jquery.countdown.min.js') ?>"></script>
<script src="<?= site_url('template/assets/vendor/slick-carousel/slick/slick.js') ?>"></script>
<script src="<?= site_url('template/assets/vendor/hs-megamenu/src/hs.megamenu.js') ?>"></script>
<script src="<?= site_url('template/assets/vendor/malihu-scrollbar/jquery.mCustomScrollbar.concat.min.js') ?>"></script>
<script src="<?= site_url('template/assets/vendor/appear.js') ?>"></script>

<!-- JS Revolution Slider -->
<script src="<?= site_url('template/assets/vendor/revolution-slider/revolution/js/jquery.themepunch.tools.min.js') ?>"></script>
<script src="<?= site_url('template/assets/vendor/revolution-slider/revolution/js/jquery.themepunch.revolution.min.js') ?>"></script>

<script src="<?= site_url('template/assets/vendor/revolution-slider/revolution-addons/typewriter/js/revolution.addon.typewriter.min.js') ?>"></script>

<script src="<?= site_url('template/assets/vendor/revolution-slider/revolution/js/extensions/revolution.extension.actions.min.js') ?>"></script>
<script src="<?= site_url('template/assets/vendor/revolution-slider/revolution/js/extensions/revolution.extension.carousel.min.js') ?>"></script>
<script src="<?= site_url('template/assets/vendor/revolution-slider/revolution/js/extensions/revolution.extension.kenburn.min.js') ?>"></script>
<script src="<?= site_url('template/assets/vendor/revolution-slider/revolution/js/extensions/revolution.extension.layeranimation.min.js') ?>"></script>
<script src="<?= site_url('template/assets/vendor/revolution-slider/revolution/js/extensions/revolution.extension.migration.min.js') ?>"></script>
<script src="<?= site_url('template/assets/vendor/revolution-slider/revolution/js/extensions/revolution.extension.navigation.min.js') ?>"></script>
<script src="<?= site_url('template/assets/vendor/revolution-slider/revolution/js/extensions/revolution.extension.parallax.min.js') ?>"></script>
<script src="<?= site_url('template/assets/vendor/revolution-slider/revolution/js/extensions/revolution.extension.slideanims.min.js') ?>"></script>
<script src="<?= site_url('template/assets/vendor/revolution-slider/revolution/js/extensions/revolution.extension.video.min.js') ?>"></script>

<!-- JS Unify -->
<script src="<?= site_url('template/assets/js/hs.core.js') ?>"></script>
<script src="<?= site_url('template/assets/js/components/hs.header.js') ?>"></script>
<script src="<?= site_url('template/assets/js/helpers/hs.hamburgers.js') ?>"></script>
<script src="<?= site_url('template/assets/js/components/hs.dropdown.js') ?>"></script>
<script src="<?= site_url('template/assets/js/components/hs.scrollbar.js') ?>"></script>
<script src="<?= site_url('template/assets/js/components/hs.countdown.js') ?>"></script>
<script src="<?= site_url('template/assets/js/components/hs.carousel.js') ?>"></script>
<script src="<?= site_url('template/assets/js/components/hs.go-to.js') ?>"></script>
<script src="<?= site_url('template/assets/js/components/hs.count-qty.js') ?>"></script>

<!-- JS Customization -->
<script src="<?= site_url('template/assets/js/custom.js')?>"></script>

