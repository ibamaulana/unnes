<!DOCTYPE html>
<html lang="en">

<?php $this->load->view('head') ?>

<body>
  <main>
      <!-- Header -->
      <?php $this->load->view('header') ?>
      <!-- End Header -->

      <!-- Promo Slider -->
      <div class="g-bg-gray-light-v5 g-pt-20 g-pb-20">
        <div class="container">
          <div class="row align-items-center g-bg-white g-py-15">
            <div class="col-md-4 g-pl-30 g-mb-40 g-mb-0--md">
              <!-- Product Info -->
              <div class="text-center">
                <div class="mb-4">
                  <h1 class="h2">Perkebunan Semarang</h1>
					Jl. Cepoko Raya, Cepoko, Gn. Pati, Kota Semarang, Jawa Tengah 50223 | 085647474045
                </div>
                <!-- End Product Info -->
              </div>

              <!-- Accordion -->
              <div id="accordion-01" >
                <div id="accordion-01-heading-01" class="g-brd-y g-brd-gray-light-v3 py-3" role="tab">
                  <h5 class="g-font-weight-400 g-font-size-default mb-0">
                    <a class="d-block g-color-gray-dark-v5 g-text-underline--none--hover">Deskripsi</a>
                  </h5>
				  <?php echo $deskripsiweb;?>
				</div>
              </div>
              <!-- End Accordion -->

              <!-- Accordion -->
              <div id="accordion-03" class="mb-3" role="tablist" aria-multiselectable="true">
                <div id="accordion-03-heading-01" class="g-brd-bottom g-brd-gray-light-v3 py-3" role="tab">
                  <h5 class="g-font-weight-400 g-font-size-default mb-0">
                    <a class="d-block g-color-gray-dark-v5 g-text-underline--none--hover">Lokasi</a>
                  </h5></br>
					<script 
						  src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAKHPN9WRfU-nvVloPpRk7ROpPoFkNNYdo&callback=initMap">
						</script>
						<!--<script src="http://maps.googleapis.com/maps/api/js"></script>-->
						<script>
							var x=new google.maps.LatLng(52.395715,4.888916);
							var home=new google.maps.LatLng(-7.072929, 110.356344);

							function initialize(){
								var mapProp = {
								  center:home,
								  zoom:17,
								  mapTypeId:google.maps.MapTypeId.ROADMAP
								};
								var map=new google.maps.Map(document.getElementById("googleMap"),mapProp);
								var marker=new google.maps.Marker({
											  position:home, animation:google.maps.Animation.BOUNCE
								});
								marker.setMap(map);
								var infowindow = new google.maps.InfoWindow({
								  content:"Jl. Cepoko Raya, Cepoko, Gn. Pati, Kota Semarang, Jawa Tengah 50223"
								});
								google.maps.event.addListener(marker, 'click', function() {
								  infowindow.open(map,marker);
								  }); 
							}
							google.maps.event.addDomListener(window, 'load', initialize);
						</script>
						<div id="googleMap" style="height:300px;"></div>
					</div>
              </div>
              <!-- End Accordion -->

              <!-- Buttons -->
              <!-- <div class="text-center">
                <button class="btn u-btn-primary g-font-size-12 text-uppercase g-py-12 g-px-25" type="button">
                  Add to Cart <i class="align-middle ml-2 icon-finance-100 u-line-icon-pro"></i>
                </button>
              </div> -->
              <!-- End Buttons -->
            </div>

            <div class="col-md-8">
              <!-- Carousel Slider -->
              <div class="js-carousel"
                   data-infinite="true"
                   data-autoplay="true"
                   data-fade="true"
                   data-speed="7000"
                   data-lazy-load="ondemand"
                   data-pagi-classes="u-carousel-indicators-v1 g-pos-abs g-bottom-20 g-left-20">
                <div class="js-slide">
                  <div class="g-bg-size-cover g-bg-pos-center g-min-height-600" data-bg-img-src="<?= site_url('assets/sawah-3.jpg') ?>"></div>
                </div>
                <div class="js-slide">
                  <div class="g-bg-size-cover g-bg-pos-center g-min-height-600" data-bg-img-src="<?= site_url('assets/sawah-2.jpeg') ?>"></div>
                </div>
                <div class="js-slide">
                  <div class="g-bg-size-cover g-bg-pos-center g-min-height-600" data-bg-img-src="<?= site_url('assets/sawah-1.jpg') ?>"></div>
                </div>
              </div>
              <!-- End Carousel Slider -->
            </div>
          </div>
        </div>
      </div>
      <!-- End Promo Slider -->

      <!-- Products -->
      <div class="container g-pb-50 g-pt-50">
        <div class="text-center mx-auto g-max-width-600 g-mb-50">
          <h2 class="g-color-black mb-4">Perkebunan Lainnya</h2>
          <p class="lead">Perkebunan potensial yang berletak di Kota Semarang.</p>
        </div>

         <!-- Products -->
              <div class="row g-pt-30 g-mb-50">
				<?php
					$tanda = 0;
					foreach($kebun->result() as $row){
						$id_kebun = $row->id_kebun;
						$nama_kebun = $row->nama_kebun;
						$deskripsi_kebun = $row->deskripsi_kebun;
						$pemilik = $row->pemilik;
						$lokasi = $row->lokasi;
						$lat = $row->lat;
						$lng = $row->lng;
						$id_gambar_kebun = $row->id_gambar_kebun;
						$path = $row->path;
				?>
                <div class="col-6 col-lg-6 g-mb-30">
                  <!-- Product -->
                  <figure class="g-pos-rel g-mb-20">
                    <img class="img-fluid" src="<?= site_url($path) ?>" alt="Image Description">
                  </figure>

                  <div class="media">
                    <!-- Product Info -->
                    <div class="d-flex flex-column">
					  <span class="d-block g-color-black g-font-size-17"><?php echo $nama_kebun;?></span>
					  <h4 class="h6 g-color-black mb-1">
						<a class="u-link-v5 g-color-black g-color-primary--hover" href="#!">
						  <?php echo $pemilik;?>
						</a>
					  </h4>
					  <a class="d-inline-block g-color-gray-dark-v5 g-font-size-13" href="#!"><?php echo $lokasi;?></a>
					  
					</div>
                    <!-- End Product Info -->

                    <!-- Products Icons -->
                    <ul class="list-inline media-body text-right">
                      <li class="list-inline-item align-middle mx-0">
                        <a class="u-icon-v1 u-icon-size--sm g-color-gray-dark-v5 g-color-primary--hover g-font-size-15 rounded-circle" target="_blank" href="<?= site_url('detail_kebun?id=');echo $id_kebun;?>"
                           data-toggle="tooltip"
                           data-placement="top"
                           title="See Detail">
                          <i class="icon-finance-132 u-line-icon-pro"></i>
                        </a>
                      </li>
                    </ul>
                    <!-- End Products Icons -->
                  </div>
                  <!-- End Product -->
                </div>
				<?php
					}
				?>
              </div>
              <!-- End Products -->
        </div>
      </div>
      <!-- End Products -->
      

      <!-- Footer -->
      <footer class="g-bg-main-light-v1">
        <!-- Content -->
        <div class="g-brd-bottom g-brd-secondary-light-v1">
          <div class="container g-pt-100">
            <div class="row justify-content-start g-mb-30 g-mb-0--md">
              <div class="col-md-5 g-mb-30">
                <h2 class="h5 g-color-gray-light-v3 mb-4">Products</h2>

                <div class="row">
                  <div class="col-4 g-mb-20">
                    <!-- Links -->
                    <ul class="list-unstyled g-font-size-13 mb-0">
                      <li class="g-mb-10">
                        <a class="u-link-v5 g-color-gray-dark-v5 g-color-primary--hover" href="#!">SmartPhone</a>
                      </li>
                      <li class="g-my-10">
                        <a class="u-link-v5 g-color-gray-dark-v5 g-color-primary--hover" href="#!">Laptop</a>
                      </li>
                      <li class="g-my-10">
                        <a class="u-link-v5 g-color-gray-dark-v5 g-color-primary--hover" href="#!">Mouse</a>
                      </li>
                      <li class="g-my-10">
                        <a class="u-link-v5 g-color-gray-dark-v5 g-color-primary--hover" href="#!">Monitor</a>
                      </li>
                      <li class="g-my-10">
                        <a class="u-link-v5 g-color-gray-dark-v5 g-color-primary--hover" href="#!">Watch</a>
                      </li>
                    </ul>
                    <!-- End Links -->
                  </div>

                  <div class="col-4 g-mb-20">
                    <!-- Links -->
                    <ul class="list-unstyled g-font-size-13 mb-0">
                      <li class="g-mb-10">
                        <a class="u-link-v5 g-color-gray-dark-v5 g-color-primary--hover" href="#!">Tablet</a>
                      </li>
                      <li class="g-my-10">
                        <a class="u-link-v5 g-color-gray-dark-v5 g-color-primary--hover" href="#!">Accessorie</a>
                      </li>
                      <li class="g-my-10">
                        <a class="u-link-v5 g-color-gray-dark-v5 g-color-primary--hover" href="#!">Mouses Pad</a>
                      </li>
                      <li class="g-my-10">
                        <a class="u-link-v5 g-color-gray-dark-v5 g-color-primary--hover" href="#!">Handset</a>
                      </li>
                      <li class="g-my-10">
                        <a class="u-link-v5 g-color-gray-dark-v5 g-color-primary--hover" href="#!">Phablet</a>
                      </li>
                    </ul>
                    <!-- End Links -->
                  </div>

                  <div class="col-4 g-mb-20">
                    <!-- Links -->
                    <ul class="list-unstyled g-font-size-13 mb-0">
                      <li class="g-mb-10">
                        <a class="u-link-v5 g-color-gray-dark-v5 g-color-primary--hover" href="#!">Speakers</a>
                      </li>
                      <li class="g-my-10">
                        <a class="u-link-v5 g-color-gray-dark-v5 g-color-primary--hover" href="#!">Camera</a>
                      </li>
                      <li class="g-my-10">
                        <a class="u-link-v5 g-color-gray-dark-v5 g-color-primary--hover" href="#!">Play Station</a>
                      </li>
                      <li class="g-my-10">
                        <a class="u-link-v5 g-color-gray-dark-v5 g-color-primary--hover" href="#!">Xbox</a>
                      </li>
                    </ul>
                    <!-- End Links -->
                  </div>
                </div>
              </div>

              <div class="col-sm-6 col-md-3 g-mb-30 g-mb-0--sm">
                <h2 class="h5 g-color-gray-light-v3 mb-4">Brands</h2>

                <div class="row">
                  <div class="col-6 g-mb-20">
                    <!-- Links -->
                    <ul class="list-unstyled g-font-size-13 mb-0">
                      <li class="g-mb-10">
                        <a class="u-link-v5 g-color-gray-dark-v5 g-color-primary--hover" href="#!">Logitech</a>
                      </li>
                      <li class="g-my-10">
                        <a class="u-link-v5 g-color-gray-dark-v5 g-color-primary--hover" href="#!">Samsung</a>
                      </li>
                      <li class="g-my-10">
                        <a class="u-link-v5 g-color-gray-dark-v5 g-color-primary--hover" href="#!">Microsoft</a>
                      </li>
                      <li class="g-my-10">
                        <a class="u-link-v5 g-color-gray-dark-v5 g-color-primary--hover" href="#!">Apple</a>
                      </li>
                    </ul>
                    <!-- End Links -->
                  </div>

                  <div class="col-6 g-mb-20">
                    <!-- Links -->
                    <ul class="list-unstyled g-font-size-13 mb-0">
                      <li class="g-mb-10">
                        <a class="u-link-v5 g-color-gray-dark-v5 g-color-primary--hover" href="#!">Huawei</a>
                      </li>
                      <li class="g-my-10">
                        <a class="u-link-v5 g-color-gray-dark-v5 g-color-primary--hover" href="#!">Motorola</a>
                      </li>
                      <li class="g-my-10">
                        <a class="u-link-v5 g-color-gray-dark-v5 g-color-primary--hover" href="#!">Sony</a>
                      </li>
                      <li class="g-my-10">
                        <a class="u-link-v5 g-color-gray-dark-v5 g-color-primary--hover" href="#!">Nokia</a>
                      </li>
                    </ul>
                    <!-- End Links -->
                  </div>
                </div>
              </div>

              <div class="col-sm-5 col-md-3 ml-auto g-mb-30 g-mb-0--sm">
                <h2 class="h5 g-color-gray-light-v3 mb-4">Contacts</h2>

                <!-- Links -->
                <ul class="list-unstyled g-color-gray-dark-v5 g-font-size-13">
                  <li class="media my-3">
                    <i class="d-flex mt-1 mr-3 icon-hotel-restaurant-235 u-line-icon-pro"></i>
                    <div class="media-body">
                      Unit 25 Suite 3, 925 Prospect
                      <br>
                      PI New York Avenue
                    </div>
                  </li>
                  <li class="media my-3">
                    <i class="d-flex mt-1 mr-3 icon-communication-062 u-line-icon-pro"></i>
                    <div class="media-body">
                      htmlstream@support.com
                    </div>
                  </li>
                  <li class="media my-3">
                    <i class="d-flex mt-1 mr-3 icon-communication-033 u-line-icon-pro"></i>
                    <div class="media-body">
                      +32 333 444 555
                    </div>
                  </li>
                </ul>
                <!-- End Links -->
              </div>
            </div>

            <!-- Secondary Content -->
            <div class="row">
              <div class="col-md-4 g-mb-50">
                <h2 class="h5 g-color-gray-light-v3 mb-4">Subscribe</h2>

                <!-- Subscribe Form -->
                <form class="input-group u-shadow-v19 rounded">
                  <input class="form-control g-brd-none g-color-gray-dark-v5 g-bg-main-light-v2 g-bg-main-light-v2--focus g-placeholder-gray-dark-v3 rounded g-px-20 g-py-8" type="email" placeholder="Enter your email">
                  <span class="input-group-addon u-shadow-v19 g-brd-none g-bg-main-light-v2 g-pa-5">
                    <button class="btn u-btn-primary rounded text-uppercase g-py-8 g-px-18" type="submit">
                      <i class="fa fa-angle-right"></i>
                    </button>
                  </span>
                </form>
                <!-- End Subscribe Form -->
              </div>

              <div class="col-6 col-md-3 offset-lg-1 g-mb-30">
                <h2 class="h5 g-color-gray-light-v3 mb-4">Language/Currency:</h2>

                <!-- Large Button Group -->
                <div class="btn-group dropup">
                  <button class="btn btn-black g-bg-main-light-v1 btn-lg g-color-gray-dark-v5 g-color-primary--hover g-font-size-default g-pl-0 mr-5" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <svg class="g-ml-minus-6" xmlns="http://www.w3.org/2000/svg" height="11" width="27" viewBox="0 0 640 480">
                      <defs>
                        <clipPath id="a">
                          <path fill-opacity=".67" d="M-85.333 0h682.67v512h-682.67z"/>
                        </clipPath>
                      </defs>
                      <g clip-path="url(#a)" transform="translate(80) scale(.94)">
                        <g stroke-width="1pt">
                          <path fill="#006" d="M-256 0H768.02v512.01H-256z"/>
                          <path d="M-256 0v57.244l909.535 454.768H768.02V454.77L-141.515 0H-256zM768.02 0v57.243L-141.515 512.01H-256v-57.243L653.535 0H768.02z" fill="#fff"/>
                          <path d="M170.675 0v512.01h170.67V0h-170.67zM-256 170.67v170.67H768.02V170.67H-256z" fill="#fff"/>
                          <path d="M-256 204.804v102.402H768.02V204.804H-256zM204.81 0v512.01h102.4V0h-102.4zM-256 512.01L85.34 341.34h76.324l-341.34 170.67H-256zM-256 0L85.34 170.67H9.016L-256 38.164V0zm606.356 170.67L691.696 0h76.324L426.68 170.67h-76.324zM768.02 512.01L426.68 341.34h76.324L768.02 473.848v38.162z" fill="#c00"/>
                        </g>
                      </g>
                    </svg>
                    English
                    <i class="g-font-size-12 ml-2 fa fa-caret-up"></i>
                  </button>
                  <div class="dropdown-menu g-brd-gray-dark-v2 g-bg-main-light-v2">
                    <a class="dropdown-item g-color-gray-dark-v5" href="#!">
                      <svg xmlns="http://www.w3.org/2000/svg" height="11" width="27" viewBox="0 0 640 480">
                        <defs>
                          <clipPath id="a">
                            <path fill-opacity=".67" d="M-85.333 0h682.67v512h-682.67z"/>
                          </clipPath>
                        </defs>
                        <g clip-path="url(#a)" transform="translate(80) scale(.94)">
                          <g stroke-width="1pt">
                            <path fill="#006" d="M-256 0H768.02v512.01H-256z"/>
                            <path d="M-256 0v57.244l909.535 454.768H768.02V454.77L-141.515 0H-256zM768.02 0v57.243L-141.515 512.01H-256v-57.243L653.535 0H768.02z" fill="#fff"/>
                            <path d="M170.675 0v512.01h170.67V0h-170.67zM-256 170.67v170.67H768.02V170.67H-256z" fill="#fff"/>
                            <path d="M-256 204.804v102.402H768.02V204.804H-256zM204.81 0v512.01h102.4V0h-102.4zM-256 512.01L85.34 341.34h76.324l-341.34 170.67H-256zM-256 0L85.34 170.67H9.016L-256 38.164V0zm606.356 170.67L691.696 0h76.324L426.68 170.67h-76.324zM768.02 512.01L426.68 341.34h76.324L768.02 473.848v38.162z" fill="#c00"/>
                          </g>
                        </g>
                      </svg>
                      English
                    </a>
                    <a class="dropdown-item g-color-gray-dark-v5" href="#!">
                      <svg xmlns="http://www.w3.org/2000/svg" height="11" width="27" viewBox="0 0 640 480">
                        <g stroke-width="1pt" fill-rule="evenodd">
                          <path fill="#fff" d="M0 0h640v480H0z"/>
                          <path fill="#00267f" d="M0 0h213.33v480H0z"/>
                          <path fill="#f31830" d="M426.67 0H640v480H426.67z"/>
                        </g>
                      </svg>
                      Spanish
                    </a>
                    <a class="dropdown-item g-color-gray-dark-v5" href="#!">
                      <svg xmlns="http://www.w3.org/2000/svg" height="11" width="27" viewBox="0 0 640 480">
                        <g fill-rule="evenodd" stroke-width="1pt">
                          <path fill="#fff" d="M0 0h640v480H0z"/>
                          <path fill="#0039a6" d="M0 160.003h640V480H0z"/>
                          <path fill="#d52b1e" d="M0 319.997h640V480H0z"/>
                        </g>
                      </svg>
                      Russian
                    </a>
                    <a class="dropdown-item g-color-gray-dark-v5" href="#!">
                      <svg xmlns="http://www.w3.org/2000/svg" height="11" width="27" viewBox="0 0 640 480">
                        <path fill="#ffce00" d="M0 320h640v160.002H0z"/>
                        <path d="M0 0h640v160H0z"/>
                        <path fill="#d00" d="M0 160h640v160H0z"/>
                      </svg>
                      German
                    </a>
                  </div>
                </div>
                <!-- End Large Button Group -->

                <!-- Large Button Group -->
                <div class="btn-group dropup">
                  <button class="btn btn-black g-bg-main-light-v1 btn-lg g-color-gray-dark-v5 g-color-primary--hover g-font-size-default g-pl-0" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="mr-2 fa fa-euro"></i>
                    Euro
                    <i class="mr-2 g-font-size-12 ml-2 fa fa-caret-up"></i>
                  </button>
                  <div class="dropdown-menu g-max-width-100 g-brd-gray-dark-v2 g-bg-main-light-v2">
                    <a class="dropdown-item g-color-gray-dark-v5" href="#!">
                      <i class="mr-2 fa fa-euro"></i>
                      Euro
                    </a>
                    <a class="dropdown-item g-color-gray-dark-v5" href="#!">
                      <i class="mr-2 fa fa-dollar"></i>
                      US Dollars
                    </a>
                    <a class="dropdown-item g-color-gray-dark-v5" href="#!">
                      <i class="mr-2 fa fa-gbp"></i>
                      GBP
                    </a>
                    <a class="dropdown-item g-color-gray-dark-v5" href="#!">
                      <i class="mr-2 fa fa-yen"></i>
                      Yen
                    </a>
                  </div>
                </div>
                <!-- End Large Button Group -->
              </div>

              <div class="col-6 col-md-3 ml-auto g-mb-30">
                <h2 class="h5 g-color-gray-light-v3 mb-4">Follow Us On:</h2>

                <!-- Social Icons -->
                <ul class="list-inline mb-50">
                  <li class="list-inline-item g-mr-2">
                    <a class="u-icon-v1 u-icon-slide-up--hover g-color-gray-dark-v4 g-color-white--hover g-bg-facebook--hover rounded" href="#!">
                      <i class="g-font-size-18 g-line-height-1 u-icon__elem-regular fa fa-facebook"></i>
                      <i class="g-color-white g-font-size-18 g-line-height-0_8 u-icon__elem-hover fa fa-facebook"></i>
                    </a>
                  </li>
                  <li class="list-inline-item g-mx-2">
                    <a class="u-icon-v1 u-icon-slide-up--hover g-color-gray-dark-v4 g-color-white--hover g-bg-twitter--hover rounded" href="#!">
                      <i class="g-font-size-18 g-line-height-1 u-icon__elem-regular fa fa-twitter"></i>
                      <i class="g-color-white g-font-size-18 g-line-height-0_8 u-icon__elem-hover fa fa-twitter"></i>
                    </a>
                  </li>
                  <li class="list-inline-item g-mx-2">
                    <a class="u-icon-v1 u-icon-slide-up--hover g-color-gray-dark-v4 g-color-white--hover g-bg-instagram--hover rounded" href="#!">
                      <i class="g-font-size-18 g-line-height-1 u-icon__elem-regular fa fa-instagram"></i>
                      <i class="g-color-white g-font-size-18 g-line-height-0_8 u-icon__elem-hover fa fa-instagram"></i>
                    </a>
                  </li>
                  <li class="list-inline-item g-mx-2">
                    <a class="u-icon-v1 u-icon-slide-up--hover g-color-gray-dark-v4 g-color-white--hover g-bg-google-plus--hover rounded" href="#!">
                      <i class="g-font-size-18 g-line-height-1 u-icon__elem-regular fa fa-google-plus"></i>
                      <i class="g-color-white g-font-size-18 g-line-height-0_8 u-icon__elem-hover fa fa-google-plus"></i>
                    </a>
                  </li>
                  <li class="list-inline-item g-mx-2">
                    <a class="u-icon-v1 u-icon-slide-up--hover g-color-gray-dark-v4 g-color-white--hover g-bg-linkedin--hover rounded" href="#!">
                      <i class="g-font-size-18 g-line-height-1 u-icon__elem-regular fa fa-linkedin"></i>
                      <i class="g-color-white g-font-size-18 g-line-height-0_8 u-icon__elem-hover fa fa-linkedin"></i>
                    </a>
                  </li>
                </ul>
                <!-- End Social Icons -->
              </div>
            </div>
            <!-- End Secondary Content -->
          </div>
        </div>
        <!-- End Content -->

        <!-- Copyright -->
        <div class="container g-pt-30 g-pb-10">
          <div class="row justify-content-between align-items-center">
            <div class="col-md-6 g-mb-20">
              <p class="g-font-size-13 mb-0">2018 &copy; Htmlstream. All Rights Reserved.</p>
            </div>

            <div class="col-md-6 text-md-right g-mb-20">
              <ul class="list-inline g-color-gray-dark-v5 g-font-size-25 mb-0">
                <li class="list-inline-item g-cursor-pointer mr-1">
                  <i class="fa fa-cc-visa" title="Visa"
                     data-toggle="tooltip"
                     data-placement="top"></i>
                </li>
                <li class="list-inline-item g-cursor-pointer mx-1">
                  <i class="fa fa-cc-paypal" title="Paypal"
                     data-toggle="tooltip"
                     data-placement="top"></i>
                </li>
                <li class="list-inline-item g-cursor-pointer mx-1">
                  <i class="fa fa-cc-mastercard" title="Master Card"
                     data-toggle="tooltip"
                     data-placement="top"></i>
                </li>
                <li class="list-inline-item g-cursor-pointer ml-1">
                  <i class="fa fa-cc-stripe" title="Stripe"
                     data-toggle="tooltip"
                     data-placement="top"></i>
                </li>
                <li class="list-inline-item g-cursor-pointer ml-1">
                  <i class="fa fa-cc-discover" title="Discover"
                     data-toggle="tooltip"
                     data-placement="top"></i>
                </li>
                <li class="list-inline-item g-cursor-pointer ml-1">
                  <i class="fa fa-cc-jcb" title="JCB"
                     data-toggle="tooltip"
                     data-placement="top"></i>
                </li>
              </ul>
            </div>
          </div>
        </div>
        <!-- End Copyright -->
      </footer>
      <!-- End Footer -->

      <!-- Go To Top -->
      <a class="js-go-to u-go-to-v2" href="#!"
         data-type="fixed"
         data-position='{
           "bottom": 15,
           "right": 15
         }'
         data-offset-top="400"
         data-compensation="#js-header"
         data-show-effect="zoomIn">
        <i class="hs-icon hs-icon-arrow-top"></i>
      </a>
      <!-- End Go To Top -->

      <!-- Modal Window -->
      <div id="modal-type-onscroll" class="js-autonomous-popup text-left g-bg-white g-pos-rel g-rounded-5" style="display: none;"
           data-modal-type="onscroll"
           data-open-effect="fadeIn"
           data-close-effect="fadeIn"
           data-breakpoint="1000"
           data-speed="500">
        <button type="button" class="close g-color-main-light-v3 g-color-primary--hover g-font-size-13 g-pos-abs g-top-15 g-right-15 g-opacity-1" onclick="Custombox.modal.close();">
          <i class="hs-icon hs-icon-close"></i>
        </button>

        <!-- Modal Window - Content -->
        <div class="g-width-720">
          <div class="row align-items-center">
            <div class="col-sm-6 g-height-350--sm g-bg-size-cover g-bg-pos-top-center g-rounded-left-5" data-bg-img-src="assets/img-temp/300x300/img1.jpg"></div>

            <div class="col-sm-6">
              <div class="g-pl-30 g-pl-20--sm g-pr-30 g-py-20">
                <!-- Info -->
                <div class="g-mb-35">
                  <h3 class="h1 g-color-primary">Subscribe</h3>
                  <p class="g-font-weight-300 g-font-size-16">Get free promotions every month!</p>
                </div>
                <!-- End Info -->

                <!-- Subscribe Form -->
                <form class="input-group u-shadow-v19 rounded g-mb-20">
                  <input class="form-control g-brd-right-none g-brd-gray-light-v4 g-color-white g-bg-main-light-v3 g-rounded-left-5 g-px-20 g-py-8" type="email" placeholder="Enter your email">
                  <span class="input-group-addon u-shadow-v19 g-brd-left-none g-brd-gray-light-v4 g-bg-main-light-v3 g-rounded-right-5 g-pa-5">
                    <button class="btn u-btn-primary rounded text-uppercase g-py-8 g-px-18" type="submit">
                      <i class="fa fa-angle-right"></i>
                    </button>
                  </span>
                </form>
                <!-- End Subscribe Form -->

                <!-- Social Icons -->
                <ul class="list-inline mb-0">
                  <li class="list-inline-item g-mx-0">
                    <a class="u-icon-v3 u-icon-size--xs g-color-main-light-v3 g-color-white--hover g-bg-white g-bg-primary--hover g-font-size-13 rounded" href="#!">
                      <i class="fa fa-facebook"></i>
                    </a>
                  </li>
                  <li class="list-inline-item g-mx-0">
                    <a class="u-icon-v3 u-icon-size--xs g-color-main-light-v3 g-color-white--hover g-bg-white g-bg-primary--hover g-font-size-13 rounded" href="#!">
                      <i class="fa fa-twitter"></i>
                    </a>
                  </li>
                  <li class="list-inline-item g-mx-0">
                    <a class="u-icon-v3 u-icon-size--xs g-color-main-light-v3 g-color-white--hover g-bg-white g-bg-primary--hover g-font-size-13 rounded" href="#!">
                      <i class="fa fa-instagram"></i>
                    </a>
                  </li>
                  <li class="list-inline-item g-mx-0">
                    <a class="u-icon-v3 u-icon-size--xs g-color-main-light-v3 g-color-white--hover g-bg-white g-bg-primary--hover g-font-size-13 rounded" href="#!">
                      <i class="fa fa-google-plus"></i>
                    </a>
                  </li>
                  <li class="list-inline-item g-mx-0">
                    <a class="u-icon-v3 u-icon-size--xs g-color-main-light-v3 g-color-white--hover g-bg-white g-bg-primary--hover g-font-size-13 rounded" href="#!">
                      <i class="fa fa-linkedin"></i>
                    </a>
                  </li>
                </ul>
                <!-- End Social Icons -->
              </div>
            </div>
          </div>
        </div>
        <!-- End Modal Window - Content -->
      </div>
      <!-- End Modal Window -->
    </main>

  <div class="u-outer-spaces-helper"></div>


  <?php $this->load->view('scripts') ?>

  <!-- JS Plugins Init. -->
<script>
  $(document).on('ready', function () {
    // initialization of carousel
    $.HSCore.components.HSCarousel.init('[class*="js-carousel"]');

    $('#carouselCus1').slick('setOption', 'responsive', [{
      breakpoint: 1200,
      settings: {
        slidesToShow: 4
      }
    }, {
      breakpoint: 992,
      settings: {
        slidesToShow: 3
      }
    }, {
      breakpoint: 768,
      settings: {
        slidesToShow: 2
      }
    }], true);
  });

  // initialization of header
  $.HSCore.components.HSHeader.init($('#js-header'));
  $.HSCore.helpers.HSHamburgers.init('.hamburger');

  // initialization of HSMegaMenu component
  $('.js-mega-menu').HSMegaMenu({
    event: 'hover',
    pageContainer: $('.container'),
    breakpoint: 991
  });

  // initialization of HSDropdown component
  $.HSCore.components.HSDropdown.init($('[data-dropdown-target]'), {
    afterOpen: function () {
      $(this).find('input[type="search"]').focus();
    }
  });

  // initialization of go to
  $.HSCore.components.HSGoTo.init('.js-go-to');

  // initialization of countdowns
  var countdowns = $.HSCore.components.HSCountdown.init('.js-countdown', {
    yearsElSelector: '.js-cd-years',
    monthElSelector: '.js-cd-month',
    daysElSelector: '.js-cd-days',
    hoursElSelector: '.js-cd-hours',
    minutesElSelector: '.js-cd-minutes',
    secondsElSelector: '.js-cd-seconds'
  });

  // initialization of quantity counter
  $.HSCore.components.HSCountQty.init('.js-quantity');

  $(window).on('load', function () {
    // initialization of HSScrollBar component
    $.HSCore.components.HSScrollBar.init($('.js-scrollbar'));
  });

  // initialization of revolution slider
  var tpj = jQuery;

  var revapi1014;
  tpj(document).ready(function () {
    if (tpj("#rev_slider_1014_1").revolution == undefined) {
      revslider_showDoubleJqueryError("#rev_slider_1014_1");
    } else {
      revapi1014 = tpj("#rev_slider_1014_1").show().revolution({
        sliderType: "standard",
        jsFileLocation: "revolution/js/",
        sliderLayout: "fullscreen",
        dottedOverlay: "none",
        delay: 9000,
        navigation: {
          keyboardNavigation: "off",
          keyboard_direction: "horizontal",
          mouseScrollNavigation: "off",
          mouseScrollReverse: "default",
          onHoverStop: "off",
          touch: {
            touchenabled: "on",
            swipe_threshold: 75,
            swipe_min_touches: 1,
            swipe_direction: "horizontal",
            drag_block_vertical: false
          }
          ,
          arrows: {
            style: "uranus",
            enable: true,
            hide_onmobile: true,
            hide_under: 768,
            hide_onleave: false,
            tmp: '',
            left: {
              h_align: "left",
              v_align: "center",
              h_offset: 20,
              v_offset: 0
            },
            right: {
              h_align: "right",
              v_align: "center",
              h_offset: 20,
              v_offset: 0
            }
          }
        },
        parallax: {
          type: "mouse",
          origo: "slidercenter",
          speed: 2000,
          levels: [2, 3, 4, 5, 6, 7, 12, 16, 10, 50],
          disable_onmobile: "on"
        },
        responsiveLevels: [1240, 1024, 778, 480],
        visibilityLevels: [1240, 1024, 778, 480],
        gridwidth: [1240, 1024, 778, 480],
        gridheight: [868, 768, 960, 600],
        lazyType: "none",
        shadow: 0,
        spinner: "off",
        stopLoop: "on",
        stopAfterLoops: 0,
        stopAtSlide: 1,
        shuffle: "off",
        autoHeight: "off",
        fullScreenAutoWidth: "off",
        fullScreenAlignForce: "off",
        fullScreenOffsetContainer: "#js-header",
        fullScreenOffset: "",
        disableProgressBar: "on",
        hideThumbsOnMobile: "off",
        hideSliderAtLimit: 0,
        hideCaptionAtLimit: 0,
        hideAllCaptionAtLilmit: 0,
        debugMode: false,
        fallbacks: {
          simplifyAll: "off",
          nextSlideOnWindowFocus: "off",
          disableFocusListener: false
        }
      });
    }

    RsTypewriterAddOn(tpj, revapi1014);
  });
</script>

</body>

</html>