<!DOCTYPE html>
<html lang="en">

<?php $this->load->view('head') ?>

<body>
  <main>
    <!-- Header -->
    <?php $this->load->view('header') ?>
    <!-- End Header -->

    <!-- Breadcrumbs -->
    <section class="g-bg-gray-light-v5 g-py-50">
      <div class="container">
        <div class="d-sm-flex text-center">
          <div class="align-self-center">
            <h2 class="h3 g-font-weight-300 w-100 g-mb-10 g-mb-0--md"><?= $title ?></h2>
          </div>

          <div class="align-self-center ml-auto">
            <ul class="u-list-inline">
              <li class="list-inline-item g-mr-5">
                <a class="u-link-v5 g-color-main g-color-primary--hover" href="<?= site_url() ?>">Home</a>
                <i class="g-color-gray-light-v2 g-ml-5">/</i>
              </li>
              <li class="list-inline-item g-color-primary">
                <span>Agenda</span>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </section>
    <!-- End Breadcrumbs -->

    <!-- Desa Binaan -->
    <section class="g-py-100 g-mb-10">
      <div class="container">
        <!-- Icon Blocks -->
        <div class="row justify-content-center g-mb-20">
          <?php
          $no = 0; 
          foreach ($agenda->result() as $ag) {
            $no++;?>
          <div class="col-lg-4 g-mb-60 g-mt-50">
          <!-- Icon Blocks -->
            <div class="u-shadow-v29 g-bg-white text-center g-rounded-5 g-px-30 g-pb-30">
              <span class="u-icon-v3 g-bg-primary g-color-white g-pull-50x-up g-rounded-5 g-mb-5">
                <p class="g-mt-10"><?= $no ?></p>
              </span>
              <h3 class="h5 g-color-black g-mb-10"><?= $ag->nama_agenda ?></h3>
              <div class="row">
                <div class="col-md-12 text-center">
                  <p class="g-color-gray-dark-v4"><i class="fa fa-map-marker g-mr-5"></i> <?= $ag->lokasi ?> <i class="fa fa-calendar g-mr-5 g-ml-5"></i> <?= date('d M Y',strtotime($ag->tanggal)); ?></p>
                </div>
              </div>
              <p class="g-color-gray-dark-v4 g-mb-20 g-mt-10"><?= substr($ag->deskripsi, 0, 50) . '...'; ?></p>
              <a class="g-font-weight-600 g-font-size-12 g-text-underline--none--hover text-uppercase" href="<?= site_url('kegiatan/detailagenda?id=').$ag->id_agenda ?>">Read More</a>      
            </div>
            <!-- End Icon Blocks -->
          </div>
        <?php }?>
          </div>
        </div>
      </div>
    </section>
    <!-- End Desa Binaan -->

    <!-- Call To Action -->
    <section class="g-bg-primary g-color-white g-pa-30 g-mt-100" style="background-image: url(../../assets/img/bg/pattern5.png);">
      <div class="d-md-flex justify-content-md-center text-center">
        <div class="align-self-md-center">
          <p class="lead g-font-weight-400 g-mr-20--md g-mb-15 g-mb-0--md">KKN UNNES TERBAIK</p>
        </div>
      </div>
    </section>
    <!-- End Call To Action -->

    <!-- Copyright Footer -->
    <footer class="g-bg-gray-dark-v1 g-color-white-opacity-0_8 g-py-20">
      <div class="container">
        <div class="row">
          <div class="col-md-8 text-center text-md-left g-mb-10 g-mb-0--md">
            <div class="d-lg-flex">
              <small class="d-block g-font-size-default g-mr-10 g-mb-10 g-mb-0--md">2018 &copy; Tim KKN Universitas Negeri Semarang.</small>
            </div>
          </div>

          <div class="col-md-4 align-self-center">
            <ul class="list-inline text-center text-md-right mb-0">
              <li class="list-inline-item g-mx-10" data-toggle="tooltip" data-placement="top" title="Facebook">
                <a href="#!" class="g-color-white-opacity-0_5 g-color-white--hover">
                  <i class="fa fa-facebook"></i>
                </a>
              </li>
              <li class="list-inline-item g-mx-10" data-toggle="tooltip" data-placement="top" title="Skype">
                <a href="#!" class="g-color-white-opacity-0_5 g-color-white--hover">
                  <i class="fa fa-skype"></i>
                </a>
              </li>
              <li class="list-inline-item g-mx-10" data-toggle="tooltip" data-placement="top" title="Linkedin">
                <a href="#!" class="g-color-white-opacity-0_5 g-color-white--hover">
                  <i class="fa fa-linkedin"></i>
                </a>
              </li>
              <li class="list-inline-item g-mx-10" data-toggle="tooltip" data-placement="top" title="Pinterest">
                <a href="#!" class="g-color-white-opacity-0_5 g-color-white--hover">
                  <i class="fa fa-pinterest"></i>
                </a>
              </li>
              <li class="list-inline-item g-mx-10" data-toggle="tooltip" data-placement="top" title="Twitter">
                <a href="#!" class="g-color-white-opacity-0_5 g-color-white--hover">
                  <i class="fa fa-twitter"></i>
                </a>
              </li>
              <li class="list-inline-item g-mx-10" data-toggle="tooltip" data-placement="top" title="Dribbble">
                <a href="#!" class="g-color-white-opacity-0_5 g-color-white--hover">
                  <i class="fa fa-dribbble"></i>
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </footer>
    <!-- End Copyright Footer -->
    <a class="js-go-to u-go-to-v1" href="#!" data-type="fixed" data-position='{
     "bottom": 15,
     "right": 15
   }' data-offset-top="400" data-compensation="#js-header" data-show-effect="zoomIn">
      <i class="hs-icon hs-icon-arrow-top"></i>
    </a>
  </main>

  <div class="u-outer-spaces-helper"></div>


  <?php $this->load->view('scripts') ?>

</body>

</html>