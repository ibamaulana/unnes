<!DOCTYPE html>
<html lang="en">

<?php $this->load->view('admin/header'); ?>
<body class="fix-header">
    <!-- ============================================================== -->
    <!-- Preloader -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
        </svg>
    </div>
    <!-- ============================================================== -->
    <!-- Wrapper -->
    <!-- ============================================================== -->
    <div id="wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <?php $this->load->view('admin/navbar'); ?>
        <!-- End Top Navigation -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <?php $this->load->view('admin/sidebar'); ?>
        <!-- ============================================================== -->
        <!-- End Left Sidebar -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page Content -->
        <!-- ============================================================== -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title"><?= $title ?></h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <h4 class="pull-right"><?= $date ?></h4>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
                <!-- ============================================================== -->
                <!-- Different data widgets -->
                <!-- ============================================================== -->
                
                <div class="row">
                    <div class="col-sm-12">
                        <div class="white-box">
                            <div class="row">
                                <div class="col-md-8">
                                    <h3 class="box-title" style="margin-top: 10px;margin-bottom: -10px">Kelola Kebun </h3>
                                </div>
                                <div class="col-md-4">
                                    <a class="pull-right" href="javascript:void(0)" data-toggle="modal" data-target="#addKebun">
                                        <span class="circle circle-sm bg-success di" data-toggle="tooltip" title="Tambah Kebun" data-placement="bottom"><i class="ti-plus"></i></span>
                                    </a>
                                </div>
                            </div>
                            <hr>
                            <div id="kebun-data">

                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->
                <?php $this->load->view('admin/modal-kebun') ?> 

                <!--row -->
                <!-- /.row -->
                <!-- ============================================================== -->
                <!-- wallet, & manage users widgets -->
                <!-- ============================================================== -->
                <!-- .row -->
                <!-- /.row -->
                <!-- ============================================================== -->
                <!-- Profile, & inbox widgets -->
                <!-- ============================================================== -->
                <!-- .row -->
                <!-- /.row -->
                <!-- ============================================================== -->
                <!-- calendar & todo list widgets -->
                <!-- ============================================================== -->

                <!-- ============================================================== -->
                <!-- Blog-component -->
                <!-- ============================================================== -->

            </div>
            <!-- /.container-fluid -->
            <?php $this->load->view('admin/footer'); ?>
        </div>
        <!-- ============================================================== -->
        <!-- End Page Content -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <?php $this->load->view('admin/scripts') ?>
    <!-- Datatable -->
    <script type="text/javascript">
        function getKebun() {
            $.ajax({
                url:'<?= site_url('KelolaCntrl/getKebun')?>',
                data:
                { 
                    send:true,
                },
                success:function(data){
                    $('#kebun-data').html(data);
                    tooltip._tooltip();
                }
            });
        }
		function getGambar(id) {
            $.ajax({
                url:'<?= site_url('KelolaCntrl/getGambarKebun')?>',
                data:
                { 
                    send:true,
                    id:id
                },
                success:function(data){
                    $('#gambar-kebun').html(data);
                    tooltip._tooltip();
                }
            });
        }
        $(document).ready(function() {
            getKebun();
            //add
            $('#add-form').submit(function(e){
                e.preventDefault();
                form = $(this);
                var formData = new FormData($(this)[0]);
                $.ajax({
                    url: '<?= site_url('KelolaCntrl/addKebun') ?>',
                    data:formData,
                    type:'POST',
                    contentType: false,
                    processData: false,
                    success:function(){
                        getKebun();
                        $('#addKebun').modal('hide');
                        notification._toast('Success','Tambah Kebun','success');
                    }
                });
            });
            $('#edit-form').submit(function(e){
                e.preventDefault();
                form = $(this);
                var formData = new FormData($(this)[0]);
                $.ajax({
                    url: '<?= site_url('KelolaCntrl/editKebun') ?>',
                    data:formData,
                    type:'POST',
                    contentType: false,
                    processData: false,
                    success:function(){
                        getKebun()
                        $('#editKebun').modal('hide');
                        notification._toast('Success','Edit Kebun','success');
                    }
                });
            });
            $('#hapus-button').on('click',function(){
                $.ajax({
                    url: '<?= site_url('KelolaCntrl/hapusKebun') ?>',
                    data:{id:id
                    },
                    success:function(){
                        getKebun();
                        $('.hapusKebun').modal('hide');
                        notification._toast('Success','Hapus Kebun','success');
                    }
                });
            });
            $('#gambarKebun').on('shown.bs.modal',function(){
                getGambar(id);
                $('#file-data').find('#id_kebun').val(id);
            });
			$('#file-data').submit(function(e){
                id = $(this).find('#id_kebun').val();
                e.preventDefault();
                form = $(this);
                var formData = new FormData($(this)[0]);
                $.ajax({
                    url: '<?= site_url('KelolaCntrl/addGambarKebun') ?>',
                    data:formData,
                    type:'POST',
                    contentType: false,
                    processData: false,
                    success:function(){
                        getGambar(id);
                        notification._toast('Success','Tambah Gambar Kebun','success');
                    }
                });
            });

        });
    </script>
	<script>
      // This example requires the Places library. Include the libraries=places
      // parameter when you first load the API. For example:
      // <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAKHPN9WRfU-nvVloPpRk7ROpPoFkNNYdo&libraries=places">

      function initMap() {
        var map = new google.maps.Map(document.getElementById('map'), {
          center: {lat: -33.8688, lng: 151.2195},
          zoom: 13
        });
        var input = /** @type {!HTMLInputElement} */(
            document.getElementById('pac-input'));

        var types = document.getElementById('type-selector');
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(types);

        var autocomplete = new google.maps.places.Autocomplete(input);
        autocomplete.bindTo('bounds', map);

        var infowindow = new google.maps.InfoWindow();
        var marker = new google.maps.Marker({
          map: map,
          anchorPoint: new google.maps.Point(0, -29)
        });

        autocomplete.addListener('place_changed', function() {
          infowindow.close();
          marker.setVisible(false);
          var place = autocomplete.getPlace();
          if (!place.geometry) {
            // User entered the name of a Place that was not suggested and
            // pressed the Enter key, or the Place Details request failed.
            window.alert("No details available for input: '" + place.name + "'");
            return;
          }

          // If the place has a geometry, then present it on a map.
          if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
          } else {
            map.setCenter(place.geometry.location);
            map.setZoom(17);  // Why 17? Because it looks good.
          }
          marker.setIcon(/** @type {google.maps.Icon} */({
            url: place.icon,
            size: new google.maps.Size(71, 71),
            origin: new google.maps.Point(0, 0),
            anchor: new google.maps.Point(17, 34),
            scaledSize: new google.maps.Size(35, 35)
          }));
          marker.setPosition(place.geometry.location);
          marker.setVisible(true);

          var address = '';
          if (place.address_components) {
            address = [
              (place.address_components[0] && place.address_components[0].short_name || ''),
              (place.address_components[1] && place.address_components[1].short_name || ''),
              (place.address_components[2] && place.address_components[2].short_name || '')
            ].join(' ');
          }

          infowindow.setContent('<div><strong>' + place.name + '</strong><br>' + address);
          infowindow.open(map, marker);
        });

        // Sets a listener on a radio button to change the filter type on Places
        // Autocomplete.
        function setupClickListener(id, types) {
          var radioButton = document.getElementById(id);
          radioButton.addEventListener('click', function() {
            autocomplete.setTypes(types);
          });
        }

        setupClickListener('changetype-all', []);
        setupClickListener('changetype-address', ['address']);
        setupClickListener('changetype-establishment', ['establishment']);
        setupClickListener('changetype-geocode', ['geocode']);
      }
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAKHPN9WRfU-nvVloPpRk7ROpPoFkNNYdo&libraries=places&callback=initMap" async defer></script>
</body>
</html>
