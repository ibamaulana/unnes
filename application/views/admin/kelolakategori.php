<!DOCTYPE html>
<html lang="en">

<?php $this->load->view('admin/header'); ?>

<body class="fix-header">
    <!-- ============================================================== -->
    <!-- Preloader -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
        </svg>
    </div>
    <!-- ============================================================== -->
    <!-- Wrapper -->
    <!-- ============================================================== -->
    <div id="wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <?php $this->load->view('admin/navbar'); ?>
        <!-- End Top Navigation -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <?php $this->load->view('admin/sidebar'); ?>
        <!-- ============================================================== -->
        <!-- End Left Sidebar -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page Content -->
        <!-- ============================================================== -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title"><?= $title ?></h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <h4 class="pull-right"><?= $date ?></h4>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
                <!-- ============================================================== -->
                <!-- Different data widgets -->
                <!-- ============================================================== -->
                
                <div class="row">
                    <div class="col-sm-12">
                        <div class="white-box">
                            <div class="row">
                                <div class="col-md-8">
                                    <h3 class="box-title" style="margin-top: 10px;margin-bottom: -10px">Kelola Kategori</h3>
                                </div>
                                <div class="col-md-4">
                                    <a class="pull-right" href="javascript:void(0)" data-toggle="modal" data-target="#addKategori">
                                        <span class="circle circle-sm bg-success di" data-toggle="tooltip" title="Tambah Kategori" data-placement="bottom"><i class="ti-plus"></i></span>
                                    </a>
                                </div>
                            </div>
                            <hr>
							<div id="kategori-data">

                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->
                <?php $this->load->view('admin/modal-kategori') ?> 

                <!--row -->
                <!-- /.row -->
                <!-- ============================================================== -->
                <!-- wallet, & manage users widgets -->
                <!-- ============================================================== -->
                <!-- .row -->
                <!-- /.row -->
                <!-- ============================================================== -->
                <!-- Profile, & inbox widgets -->
                <!-- ============================================================== -->
                <!-- .row -->
                <!-- /.row -->
                <!-- ============================================================== -->
                <!-- calendar & todo list widgets -->
                <!-- ============================================================== -->

                <!-- ============================================================== -->
                <!-- Blog-component -->
                <!-- ============================================================== -->

            </div>
            <!-- /.container-fluid -->
            <?php $this->load->view('admin/footer'); ?>
        </div>
        <!-- ============================================================== -->
        <!-- End Page Content -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <?php $this->load->view('admin/scripts') ?>
    <!-- Datatable -->
    <script type="text/javascript">
        function getKategori(status,div) {
            $.ajax({
                url:'<?= site_url('KelolaCntrl/getKategori')?>',
                data:
                { 
                    send:true,
                    status:status
                },
                success:function(data){
                    $(div).html(data);
                    tooltip._tooltip();
                }
            });
        }
        $(document).ready(function() {
            getKategori(1,'#kategori-data');
            //add
            $('#add-form').submit(function(e){
                e.preventDefault();
                form = $(this);
                var formData = new FormData($(this)[0]);
                $.ajax({
                    url: '<?= site_url('KelolaCntrl/addKategori') ?>',
                    data:formData,
                    type:'POST',
                    contentType: false,
                    processData: false,
                    success:function(){
                        getKategori(1,'#kategori-data');
                        $('#addKategori').modal('hide');
                        notification._toast('Success','Tambah Kategori','success');
                    }
                });
            });
            $('#edit-form').submit(function(e){
                e.preventDefault();
                form = $(this);
                var formData = new FormData($(this)[0]);
                $.ajax({
                    url: '<?= site_url('KelolaCntrl/editKategori') ?>',
                    data:formData,
                    type:'POST',
                    contentType: false,
                    processData: false,
                    success:function(){
                        getKategori(1,'#kategori-data');
                        $('#editKategori').modal('hide');
                        notification._toast('Success','Edit Kategori','success');
                    }
                });
            });
            $('#hapus-button').on('click',function(){
                $.ajax({
                    url: '<?= site_url('KelolaCntrl/hapusKategori') ?>',
                    data:{id:id
                    },
                    success:function(){
                        getKategori(1,'#kategori-data');
                        $('.hapusKategori').modal('hide');
                        notification._toast('Success','Hapus Kategori','success');
                    }
                });
            });
        });
    </script>
</body>
</html>
