<div id="addKategori" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModal" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Tambah Kategori</h4> </div>
            <div class="modal-body">
                <form id="add-form" action="#" method="POST">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">Nama Kategori:</label>
                            <input type="text" class="form-control" id="nama_kategori" name="nama_kategori" required> 
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="tanggal" class="control-label">Deskripsi Kategori:</label>
                            <textarea class="form-control" id="deskripsi_kategori" name="deskripsi_kategori" required> </textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-success waves-effect waves-light">Tambah</button>
            </form>
            </div>
        </div>
    </div>
</div>
<div id="editKategori" class="modal fade" tabindex="-1" role="dialog" data-backdrop="static" aria-labelledby="myModal" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Edit Kategori</h4> </div>
            <div class="modal-body">
                <form id="edit-form" action="#" method="POST">
                <div class="row">
                    <input type="hidden" name="id_kategori" id="id_kategori">
					<div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">Nama Kategori:</label>
                            <input type="text" class="form-control" id="editnama_kategori" name="editnama_kategori" required> 
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="tanggal" class="control-label">Deskripsi Kategori:</label>
                            <textarea class="form-control" id="editdeskripsi_kategori" name="editdeskripsi_kategori" required> </textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-success waves-effect waves-light">Edit</button>
            </form>
            </div>
        </div>
    </div>
</div>
<div class="modal fade hapusKategori" tabindex="-1" role="dialog" aria-labelledby="addOrder" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h5 class="modal-title">Hapus kategori</h5> </div>
            <div class="modal-body">
                <h4>Apakah anda yakin untuk menghapus kategori ?</h4>
                <small style="color: red">semua data kategori akan ikut terhapus</small>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Batal</button>
                <a type="button" class="btn btn-danger waves-effect waves-light" id="hapus-button">Hapus</a>
            </div>
        </div>
    </div>
</div>