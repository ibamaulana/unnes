<div id="addProduk" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModal" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Tambah Produk</h4> </div>
            <div class="modal-body">
                <form id="add-form" action="#" method="POST">
                <div class="row">
					<div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">Kategori:</label>
                            <select type="text" class="form-control" id="kategori" name="kategori" required>
                                <option>-- Pilih Kategori --</option>
								<?php
									foreach($kategori->result() as $key){
								?>
									<option value="<?php echo $key->id_kategori;?>"><?php echo $key->nama_kategori;?></option>
								<?php
									}
								?>
                            </select> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nama" class="control-label">Nama:</label>
                            <input type="text" class="form-control" id="nama_produk" name="nama_produk" required> 
                        </div>
                    </div>
					<div class="col-md-6">
                        <div class="form-group">
                            <label for="tanggal" class="control-label">Berat:</label>
                            <input type="number" min="1" max="1000" class="form-control" id="berat_produk" name="berat_produk" required> </textarea>
                        </div>
                    </div>
					<div class="col-md-6">
                        <div class="form-group">
                            <label for="tanggal" class="control-label">Penjual:</label>
                            <input type="text" class="form-control" id="penjual_produk" name="penjual_produk" required>
                        </div>
                    </div>
					<div class="col-md-6">
                        <div class="form-group">
                            <label for="tanggal" class="control-label">Kontak:</label>
                            <input type="number" min="1" class="form-control" id="kontak" name="kontak" required>
                        </div>
                    </div>
					<div class="col-md-12">
                        <div class="form-group">
                            <label for="tanggal" class="control-label">Deskripsi:</label>
                            <textarea class="form-control" id="deskripsi_produk" name="deskripsi_produk" required></textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-success waves-effect waves-light">Tambah</button>
            </form>
            </div>
        </div>
    </div>
</div>
<div id="editProduk" class="modal fade" tabindex="-1" role="dialog" data-backdrop="static" aria-labelledby="myModal" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Edit Produk</h4> </div>
            <div class="modal-body">
                <form id="edit-form" action="#" method="POST">
                <div class="row">
                    <input type="hidden" name="id_produk" id="id_produk">
					<div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">Kategori:</label>
                            <select type="text" class="form-control" id="editkategori" name="editkategori" required>
                                <option>-- Pilih Kategori --</option>
								<?php
									foreach($kategori->result() as $key){
								?>
									<option value="<?php echo $key->id_kategori;?>"><?php echo $key->nama_kategori;?></option>
								<?php
									}
								?>
                            </select> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nama" class="control-label">Nama:</label>
                            <input type="text" class="form-control" id="editnama_produk" name="editnama_produk" required> 
                        </div>
                    </div>
					<div class="col-md-6">
                        <div class="form-group">
                            <label for="tanggal" class="control-label">Berat:</label>
                            <input type="number" min="1" max="1000" class="form-control" id="editberat_produk" name="editberat_produk" required> </textarea>
                        </div>
                    </div>
					<div class="col-md-6">
                        <div class="form-group">
                            <label for="tanggal" class="control-label">Penjual:</label>
                            <input type="text" class="form-control" id="editpenjual_produk" name="editpenjual_produk" required>
                        </div>
                    </div>
					<div class="col-md-6">
                        <div class="form-group">
                            <label for="tanggal" class="control-label">Kontak:</label>
                            <input type="number" min="1" class="form-control" id="editkontak" name="editkontak" required>
                        </div>
                    </div>
					<div class="col-md-12">
                        <div class="form-group">
                            <label for="tanggal" class="control-label">Deskripsi:</label>
                            <textarea class="form-control" id="editdeskripsi_produk" name="editdeskripsi_produk" required></textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-success waves-effect waves-light">Edit</button>
            </form>
            </div>
        </div>
    </div>
</div>
<div class="modal fade hapusProduk" tabindex="-1" role="dialog" aria-labelledby="addOrder" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h5 class="modal-title">Hapus Produk</h5> </div>
            <div class="modal-body">
                <h4>Apakah anda yakin untuk menghapus produk ?</h4>
                <small style="color: red">semua data produk akan ikut terhapus</small>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Batal</button>
                <a type="button" class="btn btn-danger waves-effect waves-light" id="hapus-button">Hapus</a>
            </div>
        </div>
    </div>
</div>
<div id="gambarProduk" class="modal fade" tabindex="-1" role="dialog" data-backdrop="static" aria-labelledby="myModal" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Gambar Produk</h4> </div>
            <div class="modal-body">
                <form id="file-data" action="#" method="POST" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>FILE :</label>
                            <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                <div class="form-control" data-trigger="fileinput"> <i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename"></span></div> <span class="input-group-addon btn btn-default btn-file"> <span class="fileinput-new">Select file</span> <span class="fileinput-exists">Change</span>
                                <input type="file" name="fileberkas[]" multiple> </span> <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a> 
                                <input type="hidden" id="id_produk" name="id_produk">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div id="gambar-produk">

                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-success waves-effect waves-light">Tambah</button>
            </form>
            </div>
        </div>
    </div>
</div>
<div id="detailProduk" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModal" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Detail Produk</h4> </div>
            <div class="modal-body">
			<form id="detail-form" action="#">
                <div class="row">
					<div class="col-md-12">
                        <div class="form-group">
							<table>
								<tr>
									<td><b>Kategori</b></td>
									<td> &nbsp&nbsp:&nbsp&nbsp </td>
									<td><a id="detailnama_kategori"></a></td>
								</tr>
								<tr>
									<td><b>Nama</b></td>
									<td> &nbsp&nbsp:&nbsp&nbsp </td>
									<td><a id="detailnama_produk"></a></td>
								</tr>
								<tr>
									<td><b>Deskripsi</b></td>
									<td> &nbsp&nbsp:&nbsp&nbsp </td>
									<td><a id="detaildeskripsi_produk"></a></td>
								</tr>
								<tr>
									<td><b>Berat</b></td>
									<td> &nbsp&nbsp:&nbsp&nbsp </td>
									<td><a id="detailberat_produk"></a></td>
								</tr>
								<tr>
									<td><b>Penjual</b></td>
									<td> &nbsp&nbsp:&nbsp&nbsp </td>
									<td><a id="detailpenjual_produk"></a></td>
								</tr>
								<tr>
									<td><b>Kontak</b></td>
									<td> &nbsp&nbsp:&nbsp&nbsp </td>
									<td><a id="detailkontak"></a></td>
								</tr>
							</table>
                        </div>
                    </div>
                </div>
			</form>
            </div>
        </div>
    </div>
</div>