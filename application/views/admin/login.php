<!DOCTYPE html>  
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="shortcut icon" href="<?= site_url('template/assets/img/logoicon.png')?>">
<title><?= $title ?></title>
<!-- Bootstrap Core CSS -->
<link href="<?= base_url('template/admin/asset/bootstrap/dist/css/bootstrap.min.css') ?>" rel="stylesheet">
<!-- animation CSS -->
<link href="<?= base_url('template/admin/asset/css/animate.css') ?>" rel="stylesheet">
<!-- Custom CSS -->
<link href="<?= base_url('template/admin/asset/css/style.css') ?>" rel="stylesheet">
<!-- color CSS -->
<link href="<?= base_url('template/admin/asset/css/colors/default.css') ?>" id="theme"  rel="stylesheet">
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->

</head>
<body>
<!-- Preloader -->
<div class="preloader">
  <div class="cssload-speeding-wheel"></div>
</div>
<section id="wrapper" class="login-register">
  <div class="login-box">
    <div class="white-box">
      <form class="form-horizontal" id="loginform" action="<?= site_url('login-process') ?>" method="POST">
        
        <div class="form-group">
          <div class="col-xs-12 text-center">
            <div class="user-thumb text-center"> <img alt="thumbnail" src="<?= base_url('template/assets/img/dispertan2.png')?>" style="width: 200px">
			  <hr style="margin-bottom: -5px">
            </div>
          </div>
        </div>
        <div class="form-group ">
          <div class="col-xs-12">
            <label>Username:</label>
            <input class="form-control" type="text" name="username" required="" placeholder="username">
          </div>
        </div>
        <div class="form-group ">
          <div class="col-xs-12">
            <label>Password:</label>
            <input class="form-control" type="password" name="password" required="" placeholder="password">
          </div>
        </div>
        <div class="form-group text-center">
          <div class="col-xs-12">
            <button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">Login</button>
          </div>
        </div>
      </form>
      <p class="text-center">&copy DISPERTAN 2018</p>
    </div>
  </div>
</section>
<!-- jQuery -->
<script src="<?= base_url('template/admin/plugins/bower_components/jquery/dist/jquery.min.js') ?>"></script>
<!-- Bootstrap Core JavaScript -->
<script src="<?= base_url('template/admin/asset/bootstrap/dist/js/bootstrap.min.js') ?>"></script>
<!-- Menu Plugin JavaScript -->
<script src="<?= base_url('template/admin/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js') ?>"></script>

<!--slimscroll JavaScript -->
<script src="<?= base_url('template/admin/asset/js/jquery.slimscroll.js') ?>"></script>
<!--Wave Effects -->
<script src="<?= base_url('template/admin/asset/js/waves.js') ?>"></script>
<!-- Custom Theme JavaScript -->
<script src="<?= base_url('template/admin/asset/js/custom.min.js') ?>"></script>
<!--Style Switcher -->
<script src="<?= base_url('template/admin/plugins/bower_components/styleswitcher/jQuery.style.switcher.js') ?>"></script>
</body>
</html>
