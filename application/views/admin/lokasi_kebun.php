<!DOCTYPE html>
<html lang="en">

<?php $this->load->view('admin/header'); ?>
<body class="fix-header">
	<div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
        </svg>
    </div>
	
	<div id="wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <?php $this->load->view('admin/navbar'); ?>
        <!-- End Top Navigation -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <?php $this->load->view('admin/sidebar'); ?>
        <!-- ============================================================== -->
        <!-- End Left Sidebar -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page Content -->
        <!-- ============================================================== -->
		
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title"><?= $title ?></h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <h4 class="pull-right"><?= $date ?></h4>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
                <!-- ============================================================== -->
                <!-- Different data widgets -->
                <!-- ============================================================== -->
                
                <div class="row">
                    <div class="col-sm-12">
                        <div class="white-box">
							<form action="<?= site_url('admin/kelolakebun')?>" method="POST">
                            <div class="row">
                                <div class="col-md-8">
                                    <h3 class="box-title" style="margin-top: 10px;margin-bottom: -10px">Lokasi <?php echo $nama_kebun?> </h3>
                                </div>
								<div class="col-md-4">
                                    <div class="pull-right" href="javascript:void(0)">
                                        <a title="Kembali" href="<?= site_url('admin/kelolakebun')?>" class="btn btn-default waves-effect waves-light"><i class="ti-arrow-left"></i></a>
                                        <button title="Edit Lokasi" type="submit" class="btn btn-warning waves-effect waves-light"><i class="ti-pencil-alt"></i></button>
                                    </div>
                                </div>
                            </div>
                            <hr>
								<div class="row">
									<div class="col-md-12">
										<input type="hidden" name="id_kebun" id="id_kebun" value="<?php echo $id_kebun?>">
										<?php
											if($lokasi == "-"){
										?>
										<input id="pac-input" name="lokasi" class="controls" type="text" placeholder="Masukkan lokasi"/>
										<input id="lat" name="lat" type="hidden"/>
										<input id="lng" name="lng" type="hidden"/>
										<?php 
											}else{
										?>
										<input value="<?php echo $lokasi?>" id="pac-input" name="lokasi" class="controls" type="text" placeholder="Masukkan lokasi"/>
										<input id="lat" name="lat" value="<?php echo $lat?>" type="hidden"/>
										<input id="lng" name="lng" value="<?php echo $lng?>" type="hidden"/>
										<?php
											}
										?>
										<div id="type-selector" class="controls">
										  <input type="radio" name="type" id="changetype-all" checked="checked">
										  <label for="changetype-all">All</label>

										  <input type="radio" name="type" id="changetype-establishment">
										  <label for="changetype-establishment">Establishments</label>

										  <input type="radio" name="type" id="changetype-address">
										  <label for="changetype-address">Addresses</label>

										  <input type="radio" name="type" id="changetype-geocode">
										  <label for="changetype-geocode">Geocodes</label>
										</div>
										<div id="map"></div>
									</div>
								</div>
							</form>
                        </div>
                    </div>
                </div>
                <!-- /.row -->
                <?php $this->load->view('admin/modal-kebun') ?> 

            </div>
            <!-- /.container-fluid -->
            <?php $this->load->view('admin/footer'); ?>
        </div>
        <!-- ============================================================== -->
        <!-- End Page Content -->
        <!-- ============================================================== -->
    </div>
	
	
	<script>
      // This example requires the Places library. Include the libraries=places
      // parameter when you first load the API. For example:
      // <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAKHPN9WRfU-nvVloPpRk7ROpPoFkNNYdo&libraries=places">
	  			
      function initMap() {
		var lat = document.getElementById("lat").value;
		var lng = document.getElementById("lng").value;
		if(lat == 0 && lng == 0){ 
			//default di simpang lima
			var latLng = new google.maps.LatLng(-6.990290400000001, 110.42293719999998);
		}else{
			var latLng = new google.maps.LatLng(lat, lng);
		}
		
		var map = new google.maps.Map(document.getElementById('map'), {
			zoom: 17,
			center: latLng,
			mapTypeId: google.maps.MapTypeId.ROADMAP
		  });
		
		var marker = new google.maps.Marker({
          map: map,
		  position: latLng,
          anchorPoint: new google.maps.Point(0, -29)
        });
		
        var input = /** @type {!HTMLInputElement} */(
            document.getElementById('pac-input'));

        var types = document.getElementById('type-selector');
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(types);

        var autocomplete = new google.maps.places.Autocomplete(input);
        autocomplete.bindTo('bounds', map);

        var infowindow = new google.maps.InfoWindow();
        

        autocomplete.addListener('place_changed', function() {
          infowindow.close();
          marker.setVisible(false);
          var place = autocomplete.getPlace();
          if (!place.geometry) {
            // User entered the name of a Place that was not suggested and
            // pressed the Enter key, or the Place Details request failed.
            window.alert("No details available for input: '" + place.name + "'");
            return;
          }

          // If the place has a geometry, then present it on a map.
          if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
          } else {
            map.setCenter(place.geometry.location);
            map.setZoom(17);  // Why 17? Because it looks good.
          }
          marker.setIcon(/** @type {google.maps.Icon} */({
            url: place.icon,
            size: new google.maps.Size(71, 71),
            origin: new google.maps.Point(0, 0),
            anchor: new google.maps.Point(17, 34),
            scaledSize: new google.maps.Size(35, 35)
          }));
          marker.setPosition(place.geometry.location);
          marker.setVisible(true);
		  
		  var pos = marker.getPosition();
          var address = '';
		  var position = '';
          if (place.address_components) {
			position = [
				pos.lat(),
				pos.lng()
			  ].join(', ');
            address = [
              (place.address_components[0] && place.address_components[0].short_name || ''),
              (place.address_components[1] && place.address_components[1].short_name || ''),
              (place.address_components[2] && place.address_components[2].short_name || '')
            ].join(' ');
          }
		  
		  document.getElementById("lat").value = pos.lat();
		  document.getElementById("lng").value = pos.lng();
		  
          infowindow.setContent('<div><strong>' + place.name + '</strong><br>' + address + '</br>' + position);
          infowindow.open(map, marker);
        });

        // Sets a listener on a radio button to change the filter type on Places
        // Autocomplete.
        function setupClickListener(id, types) {
          var radioButton = document.getElementById(id);
          radioButton.addEventListener('click', function() {
            autocomplete.setTypes(types);
          });
        }

        setupClickListener('changetype-all', []);
        setupClickListener('changetype-address', ['address']);
        setupClickListener('changetype-establishment', ['establishment']);
        setupClickListener('changetype-geocode', ['geocode']);
      }
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAKHPN9WRfU-nvVloPpRk7ROpPoFkNNYdo&libraries=places&callback=initMap" async defer></script>
    <script src="<?= base_url('template/admin/plugins/bower_components/jquery/dist/jquery.min.js') ?>"></script>
	<!-- Bootstrap Core JavaScript -->
	<script src="<?= base_url('template/admin/asset/bootstrap/dist/js/bootstrap.min.js') ?>"></script>
	<!-- Menu Plugin JavaScript -->
	<script src="<?= base_url('template/admin/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js') ?>"></script>
	<script src="<?= base_url('template/admin/asset/js/jquery.slimscroll.js') ?>"></script>
	<script src="<?= base_url('template/admin/asset/js/waves.js') ?>"></script>
	<!-- Custom Theme JavaScript -->
	<script src="<?= base_url('template/admin/asset/js/jasny-bootstrap.js')?>"></script>
	<script src="<?= base_url('template/admin/asset/js/custom.js') ?>"></script>
	<script src="<?= base_url('template/admin/plugins/bower_components/datatables/jquery.dataTables.min.js')?>"></script>
</body>
</html>
