<div class="table-responsive">
    <table id="tabel-produk" class="display nowrap" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th class="text-center">No.</th>
                <th class="text-center">ID Produk</th>
                <th class="text-center">Nama Produk</th>
                <th class="text-center">Penjual</th>
                <th class="text-center">Kontak</th>
                <th class="text-center">Gambar</th>
                <th class="text-center">Aksi</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $no = 0; 
            foreach ($produk->result() as $key) {
                $no++;
            ?>
            <tr>
                <td class="text-center"><?= $no ?>.</td>
                <td class="text-center"><?= $key->id_produk ?></td>
                <td class="text-left"><?= $key->nama_produk ?></td>
                <td class="text-left"><?= $key->penjual ?></td>
                <td class="text-left"><?= $key->kontak ?></td>
                <td class="text-center">
                    <a href="#mymodal" data-toggle="modal" data-target="#gambarProduk" class="gambar" data-id="<?= $key->id_produk ?>">
                        <button type="button" class="btn btn-info btn-outline btn-circle btn-sm m-r-5" data-toggle="tooltip" title="Gambar Produk">
                        <i class="ti-more"></i></button>
                    </a>
                </td>
                <td class="text-center">
                    <a href="#" class="detail" data-toggle="modal" data-target="#detailProduk" id="<?= $key->id_produk ?>">     <button type="button" class="btn btn-info btn-outline btn-circle btn-sm m-r-5 dotip" data-toggle="tooltip" title="Detail Produk"><i class="ti-search"></i></button>
                    </a>
					<a href="#" class="edit" data-toggle="modal" data-target="#editProduk" id="<?= $key->id_produk ?>">     <button type="button" class="btn btn-warning btn-outline btn-circle btn-sm m-r-5 dotip" data-toggle="tooltip" title="Edit Produk"><i class="ti-pencil-alt"></i></button>
                    </a>
                    <a href="#" class="hapus" data-toggle="modal" data-target=".hapusProduk" id="<?= $key->id_produk ?>"><button type="button" class="btn btn-danger btn-outline btn-circle btn-sm m-r-5 dotip" data-toggle="tooltip" title="Hapus Produk"><i class="ti-close"></i></button></a>
                </td>
            </tr>
            <?php }?>
        </tbody>
    </table>
    
</div>
<script type="text/javascript">
    $('#tabel-produk').DataTable();
    $('.gambar').on('click', function(){
        id = $(this).attr('data-id');
    });
    $('.hapus').on('click', function(){
        id = $(this).attr('id');
    });
    $('.edit').on('click',function(){
        id = $(this).attr('id');
        form = $('#edit-form');
        $.ajax({
            url:'<?= site_url('KelolaCntrl/getDataProduk') ?>',
            data:{id:id},
            success:function(data){
                form.find("#editnama_produk").val(data['nama_produk']);
                form.find("#editdeskripsi_produk").val(data['deskripsi']);
				form.find("#editkategori").val(data['id_kategori']);
				form.find("#editberat_produk").val(data['berat']);
				form.find("#editkontak").val(data['kontak']);
				form.find("#editpenjual_produk").val(data['penjual']);
                form.find("#id_produk").val(data['id_produk']);
            }
        });
    });
	$('.detail').on('click',function(){
        id = $(this).attr('id');
        form = $('#detail-form');
        $.ajax({
            url:'<?= site_url('KelolaCntrl/getDataProduk') ?>',
            data:{id:id},
            success:function(data){
                form.find("#detailnama_produk").html(data['nama_produk']);
                form.find("#detaildeskripsi_produk").html(data['deskripsi']);
				form.find("#detailnama_kategori").html(data['nama_kategori']);
				form.find("#detailberat_produk").html(data['berat']);
				form.find("#detailkontak").html(data['kontak']);
				form.find("#detailpenjual_produk").html(data['penjual']);
                form.find("#id_produk").html(data['id_produk']);
            }
        });
    });
</script>