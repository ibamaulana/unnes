<?php if ($gambar->result() != null) {?>
<div class="row">
	<?php foreach ($gambar->result() as $key) {?>
	<div class="col-md-4 col-sm-4">
	    <div class="product-img" style="margin-top: 20px">
            <img src="<?= site_url().$key->path ?>" style="width: 180px"/>
            <div class="pro-img-overlay text-right"> <a href="javascript:void(0)" class="bg-danger hapus-gambar" id="<?= $key->id_gambar_kebun ?>" data-id="<?= $key->id_kebun ?>" style="margin-right: 20px;margin-top: 10px"><i class="ti-close"></i></a></div>
        </div>
	</div>
	<?php } ?>
</div>
<?php }else { ?>
<div class="row">
	<div class="col-md-12 text-center">
	    <h4>Tidak ada gambar di kebun ini</h4>
	</div> 
</div>
<?php }?>
<script type="text/javascript">
    $(document).ready(function() {
    	$('.hapus-gambar').on('click',function(){
            id = $(this).attr('id');
            idkebun = $(this).attr('data-id');
            $.ajax({
                url: '<?= site_url('KelolaCntrl/hapusGambarKebun') ?>',
                data:{
                	id:id
                },
                success:function(){
                    getGambar(idkebun);
                    notification._toast('Success','Hapus Kebun','success');
                }
            });
        });
    })
</script>