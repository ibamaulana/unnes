<div id="addKebun" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModal" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Tambah Kebun</h4> </div>
            <div class="modal-body">
                <form id="add-form" action="#" method="POST">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">Nama Kebun:</label>
                            <input type="text" class="form-control" id="nama_kebun" name="nama_kebun" required> 
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="tanggal" class="control-label">Deskripsi Kebun:</label>
                            <textarea class="form-control" id="deskripsi_kebun" name="deskripsi_kebun" required> </textarea>
                        </div>
                    </div>
					<div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">Pemilik:</label>
                            <input type="text" class="form-control" id="pemilik" name="pemilik" required> 
                        </div>
                    </div>
					<!--<div class="col-md-12">
                        <div class="form-group">
                            <label for="tanggal" class="control-label">Lokasi:</label>
                            <input type="text" class="form-control" id="lokasi" name="lokasi" required/>
                        </div>
                    </div>-->
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-success waves-effect waves-light">Tambah</button>
            </form>
            </div>
        </div>
    </div>
</div>
<div id="editKebun" class="modal fade" tabindex="-1" role="dialog" data-backdrop="static" aria-labelledby="myModal" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Edit Kebun</h4> </div>
            <div class="modal-body">
                <form id="edit-form" action="#" method="POST">
                <div class="row">
                    <input type="hidden" name="id_kebun" id="id_kebun">
					<div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">Nama Kebun:</label>
                            <input type="text" class="form-control" id="editnama_kebun" name="editnama_kebun" required> 
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="tanggal" class="control-label">Deskripsi Kebun:</label>
                            <textarea class="form-control" id="editdeskripsi_kebun" name="editdeskripsi_kebun" required> </textarea>
                        </div>
                    </div>
					<div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">Pemilik:</label>
                            <input type="text" class="form-control" id="editpemilik" name="editpemilik" required> 
                        </div>
                    </div>
					<!--<div class="col-md-12">
                        <div class="form-group">
                            <label for="tanggal" class="control-label">Lokasi:</label>
                            <input type="text" class="form-control" id="editlokasi" name="editlokasi" required/>
                        </div>
                    </div>-->
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-success waves-effect waves-light">Edit</button>
            </form>
            </div>
        </div>
    </div>
</div>
<div class="modal fade hapusKebun" tabindex="-1" role="dialog" aria-labelledby="addOrder" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h5 class="modal-title">Hapus Kebun</h5> </div>
            <div class="modal-body">
                <h4>Apakah anda yakin untuk menghapus kebun ?</h4>
                <small style="color: red">semua data kebun akan ikut terhapus</small>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Batal</button>
                <a type="button" class="btn btn-danger waves-effect waves-light" id="hapus-button">Hapus</a>
            </div>
        </div>
    </div>
</div>
<div id="gambarKebun" class="modal fade" tabindex="-1" role="dialog" data-backdrop="static" aria-labelledby="myModal" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Gambar Kebun</h4> </div>
            <div class="modal-body">
                <form id="file-data" action="#" method="POST" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>FILE :</label>
                            <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                <div class="form-control" data-trigger="fileinput"> <i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename"></span></div> <span class="input-group-addon btn btn-default btn-file"> <span class="fileinput-new">Select file</span> <span class="fileinput-exists">Change</span>
                                <input type="file" name="fileberkas[]" multiple> </span> <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a> 
                                <input type="hidden" id="id_kebun" name="id_kebun">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div id="gambar-kebun">

                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-success waves-effect waves-light">Tambah</button>
            </form>
            </div>
        </div>
    </div>
</div>