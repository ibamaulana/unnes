<div class="table-responsive">
    <table id="tabel-kategori" class="display nowrap" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th class="text-center">No.</th>
                <th class="text-center">ID Kategori</th>
                <th class="text-center">Nama Kategori</th>
                <th class="text-center">Deskripsi</th>
                <th class="text-center">Aksi</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $no = 0; 
            foreach ($kategori->result() as $key) {
                $no++;
            ?>
            <tr>
                <td class="text-center"><?= $no ?>.</td>
                <td class="text-center"><?= $key->id_kategori ?></td>
                <td class="text-left"><?= $key->nama_kategori ?></td>
                <td class="text-left"><?= $key->deskripsi_kategori ?></td>
                <td class="text-center">
                    <a href="#" class="edit" data-toggle="modal" data-target="#editKategori" id="<?= $key->id_kategori ?>">     <button type="button" class="btn btn-warning btn-outline btn-circle btn-sm m-r-5 dotip" data-toggle="tooltip" title="Edit Kategori"><i class="ti-pencil-alt"></i></button>
                    </a>
                    <a href="#" class="hapus" data-toggle="modal" data-target=".hapusKategori" id="<?= $key->id_kategori ?>"><button type="button" class="btn btn-danger btn-outline btn-circle btn-sm m-r-5 dotip" data-toggle="tooltip" title="Hapus Kategori"><i class="ti-close"></i></button></a>
                </td>
            </tr>
            <?php }?>
        </tbody>
    </table>
    
</div>
<script type="text/javascript">
    $('#tabel-kategori').DataTable();
    $('.gambar').on('click', function(){
        id = $(this).attr('data-id');
    });
    $('.hapus').on('click', function(){
        id = $(this).attr('id');
    });
    $('.edit').on('click',function(){
        id = $(this).attr('id');
        form = $('#edit-form');
        $.ajax({
            url:'<?= site_url('KelolaCntrl/getDataKategori') ?>',
            data:{id:id},
            success:function(data){
                form.find("#editnama_kategori").val(data['nama_kategori']);
                form.find("#editdeskripsi_kategori").val(data['deskripsi_kategori']);
                form.find("#id_kategori").val(data['id_kategori']);
            }
        });
    });
</script>