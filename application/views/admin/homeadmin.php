<!DOCTYPE html>
<html lang="en">

<?php $this->load->view('admin/header'); ?>

<body class="fix-header">
    <!-- ============================================================== -->
    <!-- Preloader -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
        </svg>
    </div>
    <!-- ============================================================== -->
    <!-- Wrapper -->
    <!-- ============================================================== -->
    <div id="wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <?php $this->load->view('admin/navbar'); ?>
        <!-- End Top Navigation -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <?php $this->load->view('admin/sidebar'); ?>
        <!-- ============================================================== -->
        <!-- End Left Sidebar -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page Content -->
        <!-- ============================================================== -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title"><?= $title ?></h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <h4 class="pull-right"><?= $date ?></h4>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
                <!-- ============================================================== -->
                <!-- Different data widgets -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-md-12 col-xs-12">
                        <div class="white-box">
                            <div class="row">
                                <div class="col-md-5">
                                    <div class="user-bg" style="height: 300px;"> 
                                        <div class="overlay-box" style="background: url('../template/admin/plugins/images/login-register.jpg');padding-bottom: 30%;margin-top:-55px;padding-top:35%;overflow: hidden;display: block;background-size: cover;background-position: top;height: 100% ">
                                            <div class="row" style="margin-top: 20px">
                                                <div class="col-sm-12 col-md-12 col-lg-12" style="">
                                                    <div class="user-content" style="margin-left: 10px">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-7">
                                    <h3 class="text-center">SELAMAT DATANG DI DISPERTAN ADMIN PANEL</h3>
                                    <div class="user-btm-box" style="margin-top: -30px">
                                        <div class="col-md-3 col-sm-6 col-xs-6 text-center" style="margin-top: 20px">
                                            <a href="<?= site_url('admin/profilweb') ?>" ><button type="button" class="btn btn-default btn-circle btn-xl" style="width: 100px;height: 100px;border-radius: 50px;font-size: 50px;background-color: #30323e"><i class="ti ti-world text-white" style=""></i></button></a>
                                            <p class="" style="font-size: 16px;margin-top: 10px">Profil Web</p>
                                          </div>
                                        <div class="col-md-3 col-sm-6 col-xs-6 text-center" style="margin-top: 20px">
                                            <a href="<?= site_url('admin/kelolakategori') ?>" ><button type="button" class="btn btn-default btn-circle btn-xl" style="width: 100px;height: 100px;border-radius: 50px;font-size: 50px;background-color: #30323e"><i class="ti ti-list text-white" style=""></i></button></a>
                                            <p class="" style="font-size: 16px;margin-top: 10px">Kelola Kategori</p>
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-xs-6 text-center" style="margin-top: 20px">
                                            <a href="<?= site_url('admin/kelolaproduk') ?>" ><button type="button" class="btn btn-default btn-circle btn-xl" style="width: 100px;height: 100px;border-radius: 50px;font-size: 50px;background-color: #30323e"><i class="ti ti-package text-white" style=""></i></button></a>
                                            <p class="" style="font-size: 16px;margin-top: 10px">Kelola Produk</p>
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-xs-6 text-center" style="margin-top: 20px">
                                            <a href="<?= site_url('admin/kelolakebun') ?>" ><button type="button" class="btn btn-default btn-circle btn-xl" style="width: 100px;height: 100px;border-radius: 50px;font-size: 50px;background-color: #30323e"><i class="ti ti-map text-white" style=""></i></button></a>
                                            <p class="" style="font-size: 16px;margin-top: 10px">Kelola Kebun</p>
                                        </div>
                                       
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                  </div>
                <!-- <div class="row">
                    <div class="col-sm-12">
                        <div class="white-box">
                            <div class="row row-in">
                                <div class="col-lg-3 col-sm-6 row-in-br">
                                    <ul class="col-in">
                                        <li>
                                            <span class="circle circle-md bg-info"><i class="ti-import"></i></span>
                                        </li>
                                        <li class="col-last">
                                            <h3 class="counter text-right m-t-15">23</h3>
                                        </li>
                                        <li class="col-middle">
                                            <h4>Pengadaan Obat</h4>
                                            <div class="progress">
                                                <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                                                    <span class="sr-only">100% Complete (success)</span>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-lg-3 col-sm-6 row-in-br  b-r-none">
                                    <ul class="col-in">
                                        <li>
                                            <span class="circle circle-md bg-info"><i class="ti-export"></i></span>
                                        </li>
                                        <li class="col-last">
                                            <h3 class="counter text-right m-t-15">76</h3>
                                        </li>
                                        <li class="col-middle">
                                            <h4>Permintaan Obat</h4>
                                            <div class="progress">
                                                <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                                                    <span class="sr-only">100% Complete (success)</span>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-lg-3 col-sm-6 row-in-br">
                                    <ul class="col-in">
                                        <li>
                                            <span class="circle circle-md bg-success"><i class=" ti-check-box"></i></span>
                                        </li>
                                        <li class="col-last">
                                            <h3 class="counter text-right m-t-15">93</h3>
                                        </li>
                                        <li class="col-middle">
                                            <h4>Persetujuan Pengadaan</h4>
                                            <div class="progress">
                                                <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                                                    <span class="sr-only">100% Complete (success)</span>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-lg-3 col-sm-6  b-0">
                                    <ul class="col-in">
                                        <li>
                                            <span class="circle circle-md bg-success"><i class="ti-check-box"></i></span>
                                        </li>
                                        <li class="col-last">
                                            <h3 class="counter text-right m-t-15">83</h3>
                                        </li>
                                        <li class="col-middle">
                                            <h4>Persetujuan Permintaan</h4>
                                            <div class="progress">
                                                <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                                                    <span class="sr-only">100% Complete (success)</span>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> -->
                
                <!--row -->
                <!-- /.row -->
                
                <!-- ============================================================== -->
                <!-- end right sidebar -->
                <!-- ============================================================== -->
            </div>
            <!-- /.container-fluid -->
            <?php $this->load->view('admin/footer'); ?>
        </div>
        <!-- ============================================================== -->
        <!-- End Page Content -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <?php $this->load->view('admin/scripts') ?>
</body>

</html>
