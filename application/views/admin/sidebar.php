<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav slimscrollsidebar">
        <div class="sidebar-head">
            <h3><span class="fa-fw open-close"><i class="ti-menu hidden-xs"></i><i class="ti-close visible-xs"></i></span> <span class="hide-menu">Menu</span></h3> </div>
        <ul class="nav" id="side-menu">
            <li class="user-pro">
                <a href="javascript:void(0);" class="waves-effect">
                    <img src="<?= base_url('template/admin/plugins/images/users/default.jpg') ?>" alt="user-img" class="img-circle">
                    <span class="hide-menu"> <?= strtoupper($this->session->userdata('nama')) ?></span>
                </a>
            </li>
            <li> <a href="<?= site_url('admin/home') ?>" class="waves-effect"><i class="ti ti-home fa-fw" data-icon="v"></i> <span class="hide-menu"> Home</span></a>
            </li>
            <li> <a href="<?= site_url('admin/profilweb') ?>" class="waves-effect"><i class="ti ti-world fa-fw" data-icon="v"></i> <span class="hide-menu"> Profil Web</span></a>
            </li>
            <li> <a href="<?= site_url('admin/kelolakategori') ?>" class="waves-effect"><i class="ti ti-list fa-fw" data-icon="v"></i> <span class="hide-menu"> Kelola Kategori</span></a>
            </li>
            <li> <a href="<?= site_url('admin/kelolaproduk') ?>" class="waves-effect"><i class="ti ti-package fa-fw" data-icon="v"></i> <span class="hide-menu"> Kelola Produk</span></a>
            </li>
            <li> <a href="<?= site_url('admin/kelolakebun') ?>" class="waves-effect"><i class="ti ti-map fa-fw" data-icon="v"></i> <span class="hide-menu"> Kelola Kebun</span></a>
            </li>
            <li class="devider"></li>
            <li><a href="#" data-toggle="modal" data-target=".logoutModal" class="waves-effect"><i class="mdi mdi-logout fa-fw"></i> <span class="hide-menu">Log out</span></a></li>
            <li class="devider"></li>
        </ul>
    </div>
</div>
<div class="modal fade logoutModal" tabindex="-1" role="dialog" aria-labelledby="addOrder" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-body">
                <h5>Apakah anda yakin untuk logout ?</h5>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Batal</button>
                <a type="button" class="btn btn-danger waves-effect waves-light" href="<?= site_url('logout') ?>">Logout</a>
            </div>
        </div>
    </div>
</div>