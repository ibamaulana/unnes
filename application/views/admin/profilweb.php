<!DOCTYPE html>
<html lang="en">

<?php $this->load->view('admin/header'); ?>

<body class="fix-header">
    <!-- ============================================================== -->
    <!-- Preloader -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
        </svg>
    </div>
    <!-- ============================================================== -->
    <!-- Wrapper -->
    <!-- ============================================================== -->
    <div id="wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <?php $this->load->view('admin/navbar'); ?>
        <!-- End Top Navigation -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <?php $this->load->view('admin/sidebar'); ?>
        <!-- ============================================================== -->
        <!-- End Left Sidebar -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page Content -->
        <!-- ============================================================== -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title"><?= $title ?></h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <h4 class="pull-right"><?= $date ?></h4>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
                <!-- ============================================================== -->
                <!-- Different data widgets -->
                <!-- ============================================================== -->
                
                <div class="row">
                    <div class="col-sm-12">
                        <div class="white-box">
                            <div class="row">
                                <div class="col-md-8">
                                    <h3 class="box-title" style="margin-top: 10px;margin-bottom: -10px">Profil Web </h3>
                                </div>
                                <div class="col-md-4">
                                    <a class="pull-right" href="javascript:void(0)" >
                                        <span class="circle circle-sm bg-success di" ><i class="ti-world"></i></span>
                                    </a>
                                </div>
                            </div>
                            <hr>
                            <form id="update-form" action="" method="POST">
                                <input type="hidden" id="id_profil" name="id_profil">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <span><label>Nama Kantor :</label>
                                            <input type="text" class="form-control" id="namakantor" name="namakantor"></span>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <span><label>Alamat Kantor :</label>
                                            <input type="text" class="form-control" id="alamatkantor" name="alamatkantor"></span>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <span><label>No. Telepon Kantor :</label>
                                            <input type="text" class="form-control" id="telpkantor" name="telpkantor"> </span>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <span><label>Kode Pos Kantor :</label>
                                            <input type="text" class="form-control" id="kodeposkantor" name="kodeposkantor"> </span>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <span><label>Deskripsi Kantor:</label>
                                            <textarea type="text" class="form-control" id="deskripsikantor" name="deskripsikantor"></textarea> </span>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <span><label>Nama Web :</label>
                                            <input type="text" class="form-control" id="namaweb" name="namaweb"></span>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <span><label>Slogan Web :</label>
                                            <input type="text" class="form-control" id="sloganweb" name="sloganweb"></span>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <span><label>Deskripsi Web :</label>
                                            <textarea type="text" class="form-control" id="deskripsiweb" name="deskripsiweb"></textarea> </span>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <span><label>Kesan Pesan Web:</label>
                                            <textarea type="text" class="form-control" id="kesanpesanweb" name="kesanpesanweb"></textarea> </span>
                                        </div>
                                    </div>
                                    <div class="col-md-12 text-right">
                                        <button type="submit" class="btn btn-success waves-effect waves-light">Edit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- /.row -->
                <!--row -->
                <!-- /.row -->
                <!-- ============================================================== -->
                <!-- wallet, & manage users widgets -->
                <!-- ============================================================== -->
                <!-- .row -->
                <div class="row">
                    <!-- col-md-9 -->
                    <!-- /col-md-3 -->
                </div>
                <!-- /.row -->
                <!-- ============================================================== -->
                <!-- Profile, & inbox widgets -->
                <!-- ============================================================== -->
                <!-- .row -->
                <!-- /.row -->
                <!-- ============================================================== -->
                <!-- calendar & todo list widgets -->
                <!-- ============================================================== -->

                <!-- ============================================================== -->
                <!-- Blog-component -->
                <!-- ============================================================== -->

            </div>
            <!-- /.container-fluid -->
            <?php $this->load->view('admin/footer'); ?>
        </div>
        <!-- ============================================================== -->
        <!-- End Page Content -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <?php $this->load->view('admin/scripts') ?>
    <!-- Datatable -->
    <script type="text/javascript">
        function getData() {
            $.ajax({
                url:'<?= site_url('WebCntrl/getData') ?>',
                data:
                { 
                    send:true
                },
                success:function(data){
                    $('#namakantor').val(data['namakantor']);
                    $('#alamatkantor').val(data['alamatkantor']);
                    $('#deskripsikantor').val(data['deskripsikantor']);
                    $('#telpkantor').val(data['telpkantor']);
                    $('#kodeposkantor').val(data['kodeposkantor']);
                    $('#namaweb').val(data['namaweb']);
                    $('#sloganweb').val(data['sloganweb']);
                    $('#deskripsiweb').val(data['deskripsiweb']);
                    $('#kesanpesanweb').val(data['kesanpesanweb']);
                    $('#id_profil').val(data['id_profil']);
                }
            });
        }
        $(document).ready(function() {
            getData();
            //add
            $('#update-form').submit(function(e){
                e.preventDefault();
                form = $(this);
                var formData = new FormData($(this)[0]);
                $.ajax({
                    url: '<?= site_url('WebCntrl/update') ?>',
                    data:formData,
                    type:'POST',
                    contentType: false,
                    processData: false,
                    success:function(){
                        getData();
                        notification._toast('Success','Update Profil','success');
                    }
                });
            });
        });
    </script>
</body>
</html>
