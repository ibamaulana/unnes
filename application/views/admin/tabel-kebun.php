<div class="table-responsive">
    <table id="tabel-kebun" class="display nowrap" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th width="5%" class="text-center">No.</th>
                <th class="text-center">Nama Kebun</th>
                <th class="text-center">Deskripsi</th>
                <th class="text-center">Pemilik</th>
                <th width="30%" class="text-center">Lokasi</th>
                <th width="5%" class="text-center">Gambar</th>
                <th width="10%" class="text-center">Aksi</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $no = 0; 
            foreach ($kebun->result() as $key) {
                $no++;
            ?>
            <tr>
                <td class="text-center"><?= $no ?>.</td>
                <td class="text-left"><?= $key->nama_kebun ?></td>
                <td class="text-left"><?= $key->deskripsi_kebun ?></td>
                <td class="text-left"><?= $key->pemilik ?></td>
                <td class="text-left"><?= $key->lokasi ?></a></td>
				<td class="text-center">
                    <a href="#mymodal" data-toggle="modal" data-target="#gambarKebun" class="gambar" data-id="<?= $key->id_kebun ?>">
                        <button type="button" class="btn btn-info btn-outline btn-circle btn-sm m-r-5" data-toggle="tooltip" title="Gambar Kebun">
                        <i class="ti-more"></i></button>
                    </a>
                </td>
                <td class="text-center">
                    <a title="Edit Lokasi" href="lokasiKebun?id=<?php echo $key->id_kebun;?>"><button type="button" class="btn btn-success btn-outline btn-circle btn-sm m-r-5 dotip" data-toggle="tooltip" title="Edit Lokasi"><i class="ti-map-alt"></i></button>
                    </a>
					<a href="#" class="edit" data-toggle="modal" data-target="#editKebun" id="<?= $key->id_kebun ?>">     <button type="button" class="btn btn-warning btn-outline btn-circle btn-sm m-r-5 dotip" data-toggle="tooltip" title="Edit Kebun"><i class="ti-pencil-alt"></i></button>
                    </a>
                    <a href="#" class="hapus" data-toggle="modal" data-target=".hapusKebun" id="<?= $key->id_kebun ?>"><button type="button" class="btn btn-danger btn-outline btn-circle btn-sm m-r-5 dotip" data-toggle="tooltip" title="Hapus Kebun"><i class="ti-close"></i></button></a>
                </td>
            </tr>
            <?php }?>
        </tbody>
    </table>
    
</div>
<script type="text/javascript">
    $('#tabel-kebun').DataTable();
	$('.gambar').on('click', function(){
        id = $(this).attr('data-id');
    });
    $('.hapus').on('click', function(){
        id = $(this).attr('id');
    });
    $('.edit').on('click',function(){
        id = $(this).attr('id');
        form = $('#edit-form');
        $.ajax({
            url:'<?= site_url('KelolaCntrl/getDataKebun') ?>',
            data:{id:id},
            success:function(data){
                form.find("#editnama_kebun").val(data['nama_kebun']);
                form.find("#editdeskripsi_kebun").val(data['deskripsi_kebun']);
                form.find("#editpemilik").val(data['pemilik']);
                form.find("#id_kebun").val(id);
            }
        });
    });
</script>