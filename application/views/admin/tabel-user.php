<div class="table-responsive">
    <table id="tabel-user" class="display nowrap" cellspacing="0" width="100%">
        <thead>
        <tr>
            <th rowspan="2" class="text-center">No.</th>
            <th rowspan="2" class="text-center">Username</th>
            <th rowspan="2" class="text-center">Nama</th>
            <th rowspan="2" class="text-center">Level Pengguna</th>
            <th rowspan="2" class="text-center">Aksi</th>
        </tr>
        </thead>
        <tbody>
        <?php
        $no = 0;
        foreach ($data->result() as $key) {
            $no++;
        ?>
            <tr>
                <td class="text-center"><?= $no ?>.</td>
                <td class="text-center"><?= $key->username ?></td>
                <td class="text-center"><?= $key->nama ?></td>
                <?php
                    if ($key->level == 1) {
                        echo '<td class="text-center"> IF </td>';
                    } elseif ($key->level == 2) {
                        echo '<td class="text-center"> DKK </td>';
                    } else {
                        echo '<td class="text-center"> Puskesmas </td>';
                    }
                ?>
                <td class="text-center">
                    <a href="#" class="edit" data-toggle="modal" data-target="" id=""><button type="button" class="btn btn-success btn-outline btn-circle btn-sm m-r-5" data-toggle="tooltip" title="Ubah User"><i class="ti-pencil"></i></button></a>
                    <a href="#" class="delete" data-toggle="modal" data-target="" id=""><button type="button" class="btn btn-danger btn-outline btn-circle btn-sm m-r-5" data-toggle="tooltip" title="Hapus User"><i class="ti-close"></i></button></a>
                </td>
            </tr>
        <?php }?>
        </tbody>
    </table>
</div>
<script type="text/javascript">
    $('#tabel-user').DataTable();
</script>