<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" sizes="16x16" href="<?= base_url('template/assets/img/logoicon.png') ?>">
    <title><?= $title ?></title>
    <!-- Bootstrap Core CSS -->
    <link href="<?= base_url('template/admin/asset/bootstrap/dist/css/bootstrap.min.css') ?>" rel="stylesheet">
    <link href="<?= base_url('template/admin/plugins/bower_components/datatables/jquery.dataTables.min.css') ?>" rel="stylesheet" type="text/css" />
    <link href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />
    <!-- Menu CSS -->
    <link href="<?= base_url('template/admin/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css') ?>" rel="stylesheet">
    <!-- toast CSS -->
    <link href="<?= base_url('template/admin/plugins/bower_components/toast-master/css/jquery.toast.css') ?>" rel="stylesheet">
    <!-- morris CSS -->
    <link href="<?= base_url('template/admin/plugins/bower_components/morrisjs/morris.css') ?>" rel="stylesheet">
    <!-- chartist CSS -->
    <link href="<?= base_url('template/admin/plugins/bower_components/chartist-js/dist/chartist.min.css') ?>" rel="stylesheet">
    <link href="<?= base_url('template/admin/plugins/bower_components/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.css') ?>" rel="stylesheet">
    <!-- Select -->
    <link href="<?= base_url('template/admin/plugins/bower_components/custom-select/custom-select.css') ?>" rel="stylesheet" type="text/css" />
    <link href="<?= base_url('template/admin/plugins/bower_components/bootstrap-select/bootstrap-select.min.css') ?>" rel="stylesheet" />
    <!-- Color picker plugins css -->
    <link href="<?= base_url('template/admin/plugins/bower_components/clockpicker/dist/jquery-clockpicker.min.css') ?>" rel="stylesheet">
    <!-- Date picker plugins css -->
    <link href="<?= base_url('template/admin/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css')?>" rel="stylesheet" type="text/css" />
    <!-- Daterange picker plugins css -->
    <link href="<?= base_url('template/admin/plugins/bower_components/timepicker/bootstrap-timepicker.min.css') ?>" rel="stylesheet">
    <link href="<?= base_url('template/admin/plugins/bower_components/bootstrap-daterangepicker/daterangepicker.css') ?>" rel="stylesheet">
    <!-- Calendar CSS -->
    <link href="<?= base_url('template/admin/plugins/bower_components/calendar/dist/fullcalendar.css') ?>" rel="stylesheet" />
    <!-- animation CSS -->
    <link href="<?= base_url('template/admin/asset/css/animate.css') ?>" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?= base_url('template/admin/asset/css/style.css') ?>" rel="stylesheet">
    <!-- color CSS -->
    <link href="<?= base_url('template/admin/asset/css/colors/default.css') ?>" id="theme" rel="stylesheet">
    <style type="text/css">
        .clockpicker-popover {
            z-index: 999999 !important;
        };
    </style>
	<style>
      /* Always set the map height explicitly to define the size of the div
       * element that contains the map. */
      #map {
        height: 400px;
      }
      .controls {
        margin-top: 10px;
        border: 1px solid transparent;
        border-radius: 2px 0 0 2px;
        box-sizing: border-box;
        -moz-box-sizing: border-box;
        height: 32px;
        outline: none;
        box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
      }

      #pac-input {
        background-color: #fff;
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
        margin-left: 12px;
        padding: 0 11px 0 13px;
        text-overflow: ellipsis;
        width: 450px;
      }

      #pac-input:focus {
        border-color: #4d90fe;
      }

      .pac-container {
        font-family: Roboto;
      }

      #type-selector {
        color: #fff;
        background-color: #4d90fe;
        padding: 5px 11px 0px 11px;
      }

      #type-selector label {
        font-family: Roboto;
        font-size: 13px;
        font-weight: 300;
      }
    </style>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>