<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Crud extends CI_Model{
	function __construct()
    {
		parent::__construct();
		$this->load->database();
	}

	function create($table, $data)
    {
		$query = $this->db->insert($table, $data);
		return $query;
	}

	function readProfil()
	{
		$this->db->select('*');
		$this->db->from('tb_profil');

		$query = $this->db->get('');

		return $query;
	}
	
	function readProduk()
	{
		$this->db->select('*');
		$this->db->from('tb_produk');
		$this->db->join('tb_kategori','tb_produk.id_kategori=tb_kategori.id_kategori');

		$query = $this->db->get('');

		return $query;
	}

	function readProdukId($id)
	{
		$this->db->select('*');
		$this->db->from('tb_produk');
		$this->db->join('tb_kategori','tb_produk.id_kategori=tb_kategori.id_kategori');
		$this->db->where('tb_produk.id_produk=',$id);

		$query = $this->db->get('');

		return $query;
	}
	
	function kebunLengkap(){
		$query = $this->db->query('SELECT * FROM tb_kebun, tb_gambar_kebun WHERE tb_kebun.id_kebun = tb_gambar_kebun.id_kebun GROUP BY tb_kebun.id_kebun');
		return $query;
	}
	
	function readKebun()
	{
		$this->db->select('*');
		$this->db->from('tb_kebun');

		$query = $this->db->get('');

		return $query;
	}

	function readKebunId($id)
	{
		$this->db->select('*');
		$this->db->from('tb_kebun');
		$this->db->where('tb_kebun.id_kebun=',$id);

		$query = $this->db->get('');

		return $query;
	}

	function readKategori()
	{
		$this->db->select('*');
		$this->db->from('tb_kategori');
		$this->db->order_by("id_kategori", "ASC");

		$query = $this->db->get('');

		return $query;
	}

	function readKategoriId($id)
	{
		$this->db->select('*');
		$this->db->from('tb_kategori');
		$this->db->where('tb_kategori.id_kategori=',$id);

		$query = $this->db->get('');

		return $query;
	}

	function readGambarProduk($id)
	{
		$this->db->select('*');
		$this->db->from('tb_gambar_produk');
		$this->db->where('id_produk=',$id);

		$query = $this->db->get('');

		return $query;
	}
	
	function readGambarKebun($id)
	{
		$this->db->select('*');
		$this->db->from('tb_gambar_kebun');
		$this->db->where('id_kebun=',$id);

		$query = $this->db->get('');

		return $query;
	}

	function checkLogin($username,$password)
	{
		$this->db->select('*');
		$this->db->from('user');
		$this->db->where('username=',$username);
		$this->db->where('password=',$password);

		$query = $this->db->get('');

		return $query;
	}

	function readUser($id)
    {
        $this->db->select('*');
        $this->db->from('user');

        $query = $this->db->get('');

        return $query;
    }

// =============================================================================================================================================================== //


	function countPanitia($table, $id){
		$this->db->select('*');
		$this->db->from($table);
		$this->db->where('id_user=', $id);

		$query = $this->db->get('');

		return $query->num_rows();
	}

	function countNetto($table, $id){
		$this->db->select('*');
		$this->db->from($table);
		$this->db->where('id_user=', $id);

		$query = $this->db->get('');

		$netto = 0;

		foreach ($query->result() as $key) {
			$netto = $netto + $key->jml_netto;
		}

		return $netto;
	}

	function readHonor($id){
		$this->db->select('*');
		$this->db->from('honor_panitia');
		$this->db->join('kategori_kepanitiaan', 'honor_panitia.id_kepanitiaan = kategori_kepanitiaan.id_kepanitiaan');
		$this->db->join('golongan', 'honor_panitia.id_golongan = golongan.id_golongan');
		$this->db->where('honor_panitia.id_user=',$id);

		$query = $this->db->get('');

		return $query;
	}

	function readHonorId($id){
		$this->db->select('*');
		$this->db->from('honor_panitia');
		$this->db->join('kategori_kepanitiaan', 'honor_panitia.id_kepanitiaan = kategori_kepanitiaan.id_kepanitiaan');
		$this->db->join('kegiatan', 'honor_panitia.id_kegiatan = kegiatan.id_kegiatan');
		$this->db->where('honor_panitia.id_kegiatan=', $id);

		$query = $this->db->get('');

		return $query;
	}
	
	function readHonorById($id){
		$this->db->select('*');
		$this->db->from('honor_panitia');
		$this->db->join('kategori_kepanitiaan', 'honor_panitia.id_kepanitiaan = kategori_kepanitiaan.id_kepanitiaan');
		$this->db->where('honor_panitia.id_honor=', $id);

		$query = $this->db->get('');

		return $query;
	}

	function readIsianId($id){
		$this->db->select('*');
		$this->db->from('isian_kegiatan');
		$this->db->join('kategori_jabatan', 'isian_kegiatan.id_jabatan = kategori_jabatan.id_jabatan');
		$this->db->join('kegiatan', 'isian_kegiatan.id_kegiatan = kegiatan.id_kegiatan');
		$this->db->join('golongan', 'isian_kegiatan.id_golongan = golongan.id_golongan');
		$this->db->where('isian_kegiatan.id_kegiatan=', $id);

		$query = $this->db->get('');

		return $query;
	}

	function readIsianById($id){
		$this->db->select('*');
		$this->db->from('isian_kegiatan');
		$this->db->join('kategori_jabatan', 'isian_kegiatan.id_jabatan = kategori_jabatan.id_jabatan');
		$this->db->where('isian_kegiatan.id_kegiatan=', $id);

		$query = $this->db->get('');

		return $query;
	}

	function readAtkId($id){
		$this->db->select('*');
		$this->db->from('isian_atk');
		$this->db->where('isian_atk.id_user=', $id);

		$query = $this->db->get('');

		return $query;
	}

	function readAtkById($id){
		$this->db->select('*');
		$this->db->from('isian_atk');
		$this->db->where('isian_atk.id_atk=', $id);

		$query = $this->db->get('');

		return $query;
	}

	function readPrlngkpnId($id){
		$this->db->select('*');
		$this->db->from('isian_perlengkapan');
		$this->db->where('isian_perlengkapan.id_user=', $id);

		$query = $this->db->get('');

		return $query;
	}

	function readPrlngkpnById($id){
		$this->db->select('*');
		$this->db->from('isian_perlengkapan');
		$this->db->where('isian_perlengkapan.id_perlengkapan=', $id);

		$query = $this->db->get('');

		return $query;
	}

	function readKonsumsiId($id){
		$this->db->select('*');
		$this->db->from('isian_konsumsi');
		$this->db->where('isian_konsumsi.id_user=', $id);

		$query = $this->db->get('');

		return $query;
	}

	function readKonsumsiById($id){
		$this->db->select('*');
		$this->db->from('isian_konsumsi');
		$this->db->where('isian_konsumsi.id_konsumsi=', $id);

		$query = $this->db->get('');

		return $query;
	}

	function readTransportasiId($id){
		$this->db->select('*');
		$this->db->from('isian_transportasi');
		$this->db->join('kota', 'isian_transportasi.id_kota = kota.id_kota');
		$this->db->where('isian_transportasi.id_user=', $id);

		$query = $this->db->get('');

		return $query;
	}

	function readTransportasiById($id){
		$this->db->select('*');
		$this->db->from('isian_transportasi');
		$this->db->join('kota', 'isian_transportasi.id_kota = kota.id_kota');
		$this->db->where('isian_transportasi.id_transportasi=', $id);

		$query = $this->db->get('');

		return $query;
	}

	function readUserId($id){
		$this->db->select('*');
		$this->db->from('user');
		$this->db->where('user.id_user!=', $id);

		$query = $this->db->get('');

		return $query;
	}

	function readUserLvl($lvl){
		$this->db->select('*');
		$this->db->from('user');
		$this->db->where('user.level=', $lvl);

		$query = $this->db->get('');

		return $query->num_rows();
	}

	function readUserById($id){
		$this->db->select('*');
		$this->db->from('user');
		$this->db->where('user.id_user=', $id);

		$query = $this->db->get('');

		return $query;
	}

	function readStockRe($date1, $date2){
		$this->db->select('tb_pengembalian.no_faktur, tb_faktur_kembali.no_faktur_kembali, tb_faktur_kembali.tanggal');
		$this->db->from('tb_pengembalian');
		$this->db->join('tb_faktur_kembali', 'tb_pengembalian.no_faktur = tb_faktur_kembali.no_faktur_kembali');
		$this->db->where('tb_faktur_kembali.tanggal <=', $date2);
		$this->db->where('tb_faktur_kembali.tanggal >=', $date1);
		$query = $this->db->get();

		return $query->num_rows();
	}

	function readKadaluarsa(){
		$this->db->select('(CURDATE()) AS tgl, tb_penerimaan.tgl_kedaluwarsa AS tgl2 ');
		$this->db->from('tb_penerimaan');
		$this->db->join('tb_farmasi', 'tb_penerimaan.kode_obat = tb_farmasi.kode');
		$this->db->join('tb_harga', 'tb_penerimaan.kode_obat = tb_harga.kode_obat');
		$this->db->join('tb_faktur', 'tb_penerimaan.no_faktur = tb_faktur.no_faktur');

		$query = $this->db->get('');

		return $query;
	}

	function readKerjasama($idusr){
		$this->db->select('*');
		$this->db->from('tb_mou');
		$this->db->join('tb_user', 'tb_mou.id_user = tb_user.id_user');
		$this->db->join('tb_kategori', 'tb_kategori.id_kategori = tb_mou.id_kategori');
		$this->db->where('tb_mou.id_user =', $idusr);
		$this->db->order_by('tb_mou.id_mou', 'DESC');

		$query = $this->db->get('');

		return $query;
	}

	function readInisiasiAll(){
		$this->db->select('*');
		$this->db->from('tb_inisiasi');
		$this->db->join('tb_user', 'tb_inisiasi.id_user = tb_user.id_user');

		$query = $this->db->get('');

		return $query;
	}

	function readMou($id){
		$this->db->select('*');
		$this->db->from('tb_mou');
		$this->db->join('tb_user', 'tb_mou.id_user = tb_user.id_user');
		$this->db->where('tb_mou.id_mou =', $id);

		$query = $this->db->get('');

		return $query;
	}

	function readKerja(){
		$this->db->select('*');
		$this->db->from('tb_mou');
		$this->db->join('tb_kategori', 'tb_mou.id_kategori = tb_kategori.id_kategori');
		$this->db->where('tb_mou.status = 1');
		$this->db->order_by('tb_mou.tahun', 'DESC');

		$query = $this->db->get('');

		return $query;
	}

	function readKerja1(){
		$this->db->select('*');
		$this->db->from('tb_mou');
		$this->db->join('tb_kategori', 'tb_mou.id_kategori = tb_kategori.id_kategori');
		$this->db->where('tb_mou.id_user =', $this->session->userdata('iduser'));

		$query = $this->db->get('');

		return $query;
	}

	function readKerjaTer($date1,$date2){
		$this->db->select('*');
		$this->db->from('tb_mou');
		$this->db->join('tb_kategori', 'tb_mou.id_kategori = tb_kategori.id_kategori');
		$this->db->where('tb_mou.tgl_usul >=', $date1);
		$this->db->where('tb_mou.tgl_usul <=', $date2);

		$query = $this->db->get('');

		return $query;
	}

	function readKerjaTer1($date1,$date2){
		$this->db->select('*');
		$this->db->from('tb_mou');
		$this->db->join('tb_kategori', 'tb_mou.id_kategori = tb_kategori.id_kategori');
		$this->db->where('tb_mou.id_user =', $this->session->userdata('iduser'));
		$this->db->where('tb_mou.tgl_usul >=', $date1);
		$this->db->where('tb_mou.tgl_usul <=', $date2);

		$query = $this->db->get('');

		return $query;
	}

	function readKerjaTer2($date1,$date2){
		$this->db->select('*');
		$this->db->from('tb_mou');
		$this->db->join('tb_kategori', 'tb_mou.id_kategori = tb_kategori.id_kategori');
		$this->db->where('tb_mou.status = 1');
		$this->db->where('tb_mou.tgl_usul >=', $date1);
		$this->db->where('tb_mou.tgl_usul <=', $date2);

		$query = $this->db->get('');

		return $query;
	}

	function readKerjasamaKat($kat){
		$this->db->select('*');
		$this->db->from('tb_mou');
		$this->db->join('tb_kategori', 'tb_kategori.id_kategori = tb_mou.id_kategori');
		$this->db->where('tb_mou.tahun =', $thn);


		$query = $this->db->get('');

		return $query;
	}


	function readKategoriThn($thn){
		$this->db->select('*');
		$this->db->from('tb_mou');
		$this->db->join('tb_kategori', 'tb_kategori.id_kategori = tb_mou.id_kategori');
		$this->db->where('tb_mou.tahun =', $thn);

		$query = $this->db->get('');

		return $query;
	}

	function readKerjasamaId($id){
		$this->db->select('*');
		$this->db->from('tb_mou');
		$this->db->join('tb_user', 'tb_mou.id_user = tb_user.id_user');
		$this->db->join('tb_kategori', 'tb_kategori.id_kategori = tb_mou.id_kategori');
		$this->db->where('tb_mou.id_mou =', $id);

		$query = $this->db->get('');

		return $query;
	}

	function readProses($id){
		$this->db->select('*');
		$this->db->from('tb_detail');
		$this->db->join('tb_kegiatan', 'tb_detail.id_kegiatan = tb_kegiatan.id_kegiatan');
		$this->db->join('tb_proses', 'tb_detail.id_proses = tb_proses.id_proses');
		$this->db->join('tb_user', 'tb_detail.id_user = tb_user.id_user');
		$this->db->where('tb_detail.id_mou =', $id);

		$query = $this->db->get('');

		return $query;
	}

	function readKerjasamaAll(){
		$this->db->select('*');
		$this->db->from('tb_mou');
		$this->db->join('tb_user', 'tb_mou.id_user = tb_user.id_user');
		$this->db->join('tb_kategori', 'tb_kategori.id_kategori = tb_mou.id_kategori');
		$this->db->order_by('tb_mou.id_mou', 'DESC');

		$query = $this->db->get('');

		return $query;
	}

	function readKerjasamaAll1(){
		$this->db->select('*');
		$this->db->from('tb_mou');
		$this->db->join('tb_user', 'tb_mou.id_user = tb_user.id_user');
		$this->db->join('tb_kategori', 'tb_kategori.id_kategori = tb_mou.id_kategori');
		$this->db->where('tb_mou.id_user =', $this->session->userdata('iduser'));
		$this->db->order_by('tb_mou.tahun', 'DESC');

		$query = $this->db->get('');

		return $query;
	}

	function readDash($kat,$date1,$date2){
		$this->db->select('*');
		$this->db->from('tb_mou');
		$this->db->join('tb_kategori', 'tb_mou.id_kategori = tb_kategori.id_kategori');
		$this->db->where('tb_kategori.kategori =', $kat);
		$this->db->where('tb_mou.tgl_usul >=', $date1);
		$this->db->where('tb_mou.tgl_usul <=', $date2);

		$query = $this->db->get('');

		return $query->num_rows();
	}

	function readSurv1($nilai,$lvl){
		$this->db->select('*');
		$this->db->from('tb_survei');
		$this->db->join('tb_user','tb_user.id_user = tb_survei.id_user');
		$this->db->where('tb_survei.sur1 =', $nilai);
		$this->db->where('tb_user.level =', $lvl);

		$query = $this->db->get('');

		return $query->num_rows();
	}

	function readSurv2($nilai,$lvl){
		$this->db->select('*');
		$this->db->from('tb_survei');
		$this->db->join('tb_user','tb_user.id_user = tb_survei.id_user');
		$this->db->where('tb_survei.sur2 =', $nilai);
		$this->db->where('tb_user.level =', $lvl);

		$query = $this->db->get('');

		return $query->num_rows();
	}

	function readSurv3($nilai,$lvl){
		$this->db->select('*');
		$this->db->from('tb_survei');
		$this->db->join('tb_user','tb_user.id_user = tb_survei.id_user');
		$this->db->where('tb_survei.sur3 =', $nilai);
		$this->db->where('tb_user.level =', $lvl);

		$query = $this->db->get('');

		return $query->num_rows();
	}

	function readSurv4($nilai,$lvl){
		$this->db->select('*');
		$this->db->from('tb_survei');
		$this->db->join('tb_user','tb_user.id_user = tb_survei.id_user');
		$this->db->where('tb_survei.sur4 =', $nilai);
		$this->db->where('tb_user.level =', $lvl);

		$query = $this->db->get('');

		return $query->num_rows();
	}

	function readSurv5($nilai,$lvl){
		$this->db->select('*');
		$this->db->from('tb_survei');
		$this->db->join('tb_user','tb_user.id_user = tb_survei.id_user');
		$this->db->where('tb_survei.sur5 =', $nilai);
		$this->db->where('tb_user.level =', $lvl);

		$query = $this->db->get('');

		return $query->num_rows();
	}

	function readSurv6($nilai,$lvl){
		$this->db->select('*');
		$this->db->from('tb_survei');
		$this->db->join('tb_user','tb_user.id_user = tb_survei.id_user');
		$this->db->where('tb_survei.sur6 =', $nilai);
		$this->db->where('tb_user.level =', $lvl);

		$query = $this->db->get('');

		return $query->num_rows();
	}

	function readSurv7($nilai,$lvl){
		$this->db->select('*');
		$this->db->from('tb_survei');
		$this->db->join('tb_user','tb_user.id_user = tb_survei.id_user');
		$this->db->where('tb_survei.sur7 =', $nilai);
		$this->db->where('tb_user.level =', $lvl);

		$query = $this->db->get('');

		return $query->num_rows();
	}

	function readSurv8($nilai,$lvl){
		$this->db->select('*');
		$this->db->from('tb_survei');
		$this->db->join('tb_user','tb_user.id_user = tb_survei.id_user');
		$this->db->where('tb_survei.sur8 =', $nilai);
		$this->db->where('tb_user.level =', $lvl);

		$query = $this->db->get('');

		return $query->num_rows();
	}

	function readDash1($kat,$date1,$date2){
		$this->db->select('*');
		$this->db->from('tb_mou');
		$this->db->join('tb_kategori', 'tb_mou.id_kategori = tb_kategori.id_kategori');
		$this->db->where('tb_mou.id_user =', $this->session->userdata('iduser'));
		$this->db->where('tb_kategori.kategori =', $kat);
		$this->db->where('tb_mou.tgl_usul >=', $date1);
		$this->db->where('tb_mou.tgl_usul <=', $date2);

		$query = $this->db->get('');

		return $query->num_rows();
	}

	function readDash2($kat,$date1,$date2){
		$this->db->select('*');
		$this->db->from('tb_mou');
		$this->db->join('tb_kategori', 'tb_mou.id_kategori = tb_kategori.id_kategori');
		$this->db->where('tb_kategori.kategori =', $kat);
		$this->db->where('tb_mou.tgl_usul >=', $date1);
		$this->db->where('tb_mou.tgl_usul <=', $date2);
		$this->db->where('tb_mou.status = 1');

		$query = $this->db->get('');

		return $query->num_rows();
	}


	function readMasukTot($date1, $date2){
		$this->db->select('SUM(tb_penerimaan.jumlah * tb_harga.harga) AS total');
		$this->db->from('tb_penerimaan');
		$this->db->join('tb_harga', 'tb_penerimaan.kode_obat = tb_harga.kode_harga');
		$this->db->join('tb_faktur', 'tb_penerimaan.no_faktur = tb_faktur.no_faktur');
		$this->db->where('tb_faktur.tgl_faktur <=', $date2);
		$this->db->where('tb_faktur.tgl_faktur >=', $date1);
		$query = $this->db->get('');

		foreach ($query->result() as $result){
			$total = $result->total;
		}
		return $total;
	}

	function readTabMasuk($date1, $date2){
		$this->db->select('tb_penerimaan.kode_obat AS kode, tb_farmasi.nama AS obat, tb_penerimaan.jumlah AS jumlah, tb_harga.harga AS harga, tb_faktur.tgl_faktur AS tgl, tb_faktur.sumber_dana AS dana');
		$this->db->from('tb_penerimaan');
		$this->db->join('tb_harga', 'tb_penerimaan.kode_obat = tb_harga.kode_harga');
		$this->db->join('tb_faktur', 'tb_penerimaan.no_faktur = tb_faktur.no_faktur');
		$this->db->join('tb_farmasi', 'tb_penerimaan.kode_obat = tb_harga.kode_harga AND tb_harga.kode_obat=tb_farmasi.kode');
		$this->db->where('tb_faktur.tgl_faktur <=', $date2);
		$this->db->where('tb_faktur.tgl_faktur >=', $date1);
		$query = $this->db->get();

		return $query;
	}

	function readMasuk($date1, $date2, $dana){
		$this->db->select('SUM(tb_penerimaan.jumlah * tb_harga.harga) AS total');
		$this->db->from('tb_penerimaan');

		$this->db->join('tb_harga', 'tb_penerimaan.kode_obat = tb_harga.kode_harga');
		$this->db->join('tb_faktur', 'tb_penerimaan.no_faktur = tb_faktur.no_faktur');
		$this->db->where('tb_faktur.tgl_faktur <=', $date2);
		$this->db->where('tb_faktur.tgl_faktur >=', $date1);
		$this->db->where('tb_faktur.sumber_dana =', $dana);
		$query = $this->db->get();

		foreach ($query->result() as $result){
			$harga = $result->total;
		}
		return $harga;
	}

	function readKembali($date1, $date2){
		$this->db->select('tb_pengembalian.no_faktur, tb_faktur_kembali.no_faktur_kembali, tb_faktur_kembali.tanggal');
		$this->db->from('tb_pengembalian');
		$this->db->join('tb_faktur_kembali', 'tb_pengembalian.no_faktur = tb_faktur_kembali.no_faktur_kembali');
		$this->db->where('tb_faktur_kembali.tanggal <=', $date2);
		$this->db->where('tb_faktur_kembali.tanggal >=', $date1);
		$query = $this->db->get();

		return $query->num_rows();
	}

	function readKeluar($date1, $date2){
		$this->db->select('tb_pengeluaran.no_faktur_keluar, tb_faktur_keluar.no_sbbk, tb_faktur_keluar.tanggal');
		$this->db->from('tb_pengeluaran');
		$this->db->join('tb_faktur_keluar', 'tb_pengeluaran.no_faktur_keluar = tb_faktur_keluar.no_sbbk');
		$this->db->where('tb_faktur_keluar.tanggal <=', $date2);
		$this->db->where('tb_faktur_keluar.tanggal >=', $date1);
		$query = $this->db->get();

		return $query->num_rows();
	}

	function read($table, $cond, $ordField, $ordType){
		if($cond!=null){
			$this->db->where($cond);
		}
		if($ordField!=null){
			$this->db->order_by($ordField, $ordType);
		}
		$query = $this->db->get($table);
		return $query;
	}

	function readPaging($table, $cond, $ordField, $ordType, $limit, $start){
		if($cond!=null){
			$this->db->where($cond);
		}
		if($ordField!=null){
			$this->db->order_by($ordField, $ordType);
		}
		$query = $this->db->get($table, $limit, $start);
		return $query;
	}

	function totalData($table){
		$query = $this->db->get($table);
		return $query->num_rows();
	}

	function update($cond, $table, $data){
		$this->db->where($cond);
		$query = $this->db->update($table, $data);
		return $query;
	}

	function delete($cond, $table){
		$this->db->where($cond);
		$query = $this->db->delete($table);
		return $query;
	}
}