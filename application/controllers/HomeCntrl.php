<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class HomeCntrl extends MY_Controller {
	public function __construct()
    {
		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('url_helper');
		$this->load->helper('form');
		$this->load->helper('file');
		$this->load->model('Crud');
	}

	public function index()
    {
		$id=$this->session->userdata('iduser');
		$data =[
			'title' => 'DISPERTAN | Home',
			'date' => date("l, d-m-Y", strtotime("now")),
			
		];
		$this->load->view('admin/homeadmin', $data);
	}

	public function getTabel()
	{
		$id = $this->session->userdata('iduser');
		$status = $this->input->get('status');

		$data = [
			'tabel' => $this->Crud->readPengadaan($id,$status)
		];	

		if ($status == 1) {
			return $this->load->view('rencana-pengadaan', $data);	
		}else{
			return $this->load->view('tabel-pengadaan', $data);	
		}
		
	}

	public function getObat()
    {
		$id = $this->session->userdata('iduser');
		if ($this->session->has_userdata('session_obat')) {
			$session = $this->session->userdata('session_obat');
		}else{
			$session = gmdate("dmyHis", time()+3600*7).$id;
			$this->session->set_userdata('session_obat', $session);
		}

		$data = [
			'sessionobat' => $this->Crud->readObat($session),
			'session' => $session
		];
		return $this->load->view('tabel-obat', $data);
	}

	public function getEditObat()
    {
		$id = $this->input->get('id');

		$data = [
			'sessionobat' => $this->Crud->readEditObat($id)
		];

		return $this->load->view('tabel-editobat', $data);
	}

	public function getRencana()
    {
		$id_user = $this->session->userdata('iduser');
		$id = $this->input->get('id');

		$data = [
			'obat' => $this->Crud->readObatId($id,$id_user),
			'detail' => $this->Crud->readPengadaanId($id,$id_user)
		];

		return $this->load->view('rincian-pengadaan1', $data);
	}

	public function getData(){
		$id = $this->input->get('id');
		$user = $this->session->userdata('iduser');

		$query = $this->Crud->readPengadaanId($id,$user);

		foreach($query->result() as $result){
			$data = [
				'id_pengadaan'=>$result->id_pengadaan,
				'judul_pengadaan'=>$result->judul_pengadaan,
				'tgl_pengadaan'=>$result->tgl_pengadaan
			];
		}

		header('Content-Type: application/json');
		echo json_encode($data);
	}

	public function tambahObat()
	{
		$param = [
			'id_obat' => $this->input->get('obat'),
			'obat_lain' => $this->input->get('obatlain'),
			'id_satuan' => $this->input->get('satuan'),
			'jumlah' => $this->input->get('jumlah'),
			'stok' => $this->input->get('stok'),
			'id_session' => $this->session->userdata('session_obat')
		];

		$this->Crud->create('session_obat', $param);
	}

	public function hapusObat()
	{
		$id = $this->input->get('id');

		$this->Crud->delete(array('id'=>$id), 'session_obat');
	}

	public function tambahEditObat()
	{
		$id= $this->input->get('id');

		$param = [
			'id_obat' => $this->input->get('obat'),
			'obat_lain' => $this->input->get('obatlain'),
			'id_satuan' => $this->input->get('satuan'),
			'jumlah' => $this->input->get('jumlah'),
			'stok' => $this->input->get('stok'),
			'id_session' => $id,
			'status' => 2
		];

		$this->Crud->create('session_obat', $param);
	}

	public function hapusEditObat()
	{
		$id = $this->input->get('id');

		$data =[
			'status'   => 1,
		];

		$update = $this->Crud->update(array('id'=>$id), 'session_obat', $data);
	}

	public function batalEditObat()
	{
		$id = $this->input->get('id');

		$data =[
			'status'   => 0,
		];

		$update = $this->Crud->update(array('id_session'=>$id,'status'=>1), 'session_obat', $data);
	}

	public function tambahPengadaan()
	{
		$idsession = $this->session->userdata('session_obat');

		$param = [
			'id_pengadaan' => $idsession,
			'judul_pengadaan' => $this->input->post('judul'),
			'id_user' => $this->session->userdata('iduser'),
			'tgl_pengadaan' => $this->input->post('tanggal'),
			'acc_if' => 1,
			'acc_dkk' => 1,
			'status' => 1,
		];

		$this->Crud->create('pengadaan', $param);

		$dataobat = $this->Crud->readObat($idsession);

		foreach ($dataobat->result() as $key) {
			$param = [
				'id_pengadaan' => $idsession,
				'id_user' => $this->session->userdata('iduser'),
				'id_obat' => $key->id_obat,
				'id_satuan' => $key->id_satuan,
				'jumlah' => $key->jumlah,
				'stok' => $key->stok,
				'obat_lain' => $key->obat_lain,
				'acc_if' => 1,
				'acc_dkk' => 1
			];

			$this->Crud->create('detail_pengadaan', $param);
		}

		$this->session->unset_userdata('session_obat');
	}

	// untuk get pengadaan dari model ::wawan
	public function getPengadaan()
	{
		$id = $this->input->get('id');

		$data = [
            'dataku' => "My data",
			'rincian' => $this->Crud->rincianPengadaan($id)
        ];

		return $this->load->view('tabel-pengadaan', $data);
	}

	// untuk get rincian obat darimodel ::wawan
    public function rincianObat()
    {
		$id = $this->input->get('id');

		$data = [
			'detail' => $this->Crud->detailPengObat($id)
		];

		return $this->load->view('tabel-rincian-pengadaan', $data);
    }


// ========================================================================================================================================= //


	public function getHonor(){
		$id=$this->session->userdata('iduser');

		$data = [
			'honor' => $this->Crud->readHonor($id),
		];
		
		return $this->load->view('tabelhonor', $data);
	}

	public function getDataHonor(){
		$id = $this->input->get('id');

		$query = $this->Crud->readHonorById($id);
		foreach($query->result() as $result){
			$data = [
				'nama_honor'=>$result->nama_honor,
				'id_kepanitiaan'=>$result->id_kepanitiaan,
				'golongan'=>$result->id_golongan,
				'npwp'=>$result->npwp,
				'uraian'=>$result->uraian,
				'satuan'=>$result->satuan,
				'jml_brutto'=>$result->jml_brutto,
				'pajak'=>$result->pajak,
				'jml_netto'=>$result->jml_netto,
				'id_honor'=>$id,
			];
		}

		header('Content-Type: application/json');
		echo json_encode($data);
	}

	public function ubahStatus($id)
	{
		$data =[
			'acc_if'   => $this->input->post('status'),
			'pesan_if'  => $this->input->post('pesan'),
		];

		$update = $this->Crud->update(array('id_pengadaan'=>$id), 'pengadaan', $data);
	}

	public function editHonor()
	{
		$id = $this->input->post('idhonor');

		$data =[
			'nama_honor'   => $this->input->post('editnama'),
			'id_kepanitiaan'  => $this->input->post('editkategori'),
			'id_golongan' => $this->input->post('editgolongan'),
			'npwp' => $this->input->post('editnpwp'),
			'satuan' => $this->input->post('editsatuan'),
			'jml_brutto' => $this->input->post('editbrutto'),
			'pajak' => $this->input->post('editpajak'),
			'jml_netto' => $this->input->post('editnetto')
		];

		$update = $this->Crud->update(array('id_honor'=>$id), 'honor_panitia', $data);
		if($update){
			echo 1;
		}else{
			echo 2;
		}
	}

	public function deleteHonor()
	{
		$id = $this->input->get('id');

		$delete = $this->Crud->delete(array('id_honor'=>$id), 'honor_panitia');
	}

	public function getCount()
	{
		$id = $this->input->get('id');

		$data = [
			'totalpanitia' => $this->Crud->countPanitia('honor_panitia',$id),
			'totalnetto' => $this->Crud->countNetto('honor_panitia',$id)
		];

		return $this->load->view('counthonor', $data);
	}

	public function getIsian(){
		$id = $this->input->get('id');

		$data = [
			'isian' => $this->Crud->readIsianId($id),
		];
		
		return $this->load->view('tabelisian', $data);
	}

	public function tambahIsian(){
		$param = [
			'nama_isian' => $this->input->post('nama_isian'),
			'id_jabatan' => $this->input->post('jabatan'),
			'golongan' => $this->input->post('golongan_isian'),
			'npwp' => $this->input->post('npwp_isian'),
			'satuan' => $this->input->post('satuan_isian'),
			'jml_brutto' => $this->input->post('jml_brutto_isian'),
			'pajak' => $this->input->post('pajak_isian'),
			'jml_netto' => $this->input->post('jml_netto_isian'),
			'id_kegiatan' => $this->input->post('id_kegiatan_isian'),
		];
		$this->Crud->create('isian_kegiatan', $param);
	}

	public function getDataIsian(){
		$id = $this->input->get('id');

		$query = $this->Crud->readIsianById($id);
		foreach($query->result() as $result){
			$data = [
				'nama_isian'=>$result->nama_isian,
				'id_jabatan'=>$result->id_jabatan,
				'golongan'=>$result->golongan,
				'npwp'=>$result->npwp,
				'satuan'=>$result->satuan,
				'jml_brutto'=>$result->jml_brutto,
				'pajak'=>$result->pajak,
				'jml_netto'=>$result->jml_netto,
				'id_isian'=>$id,
			];
		}

		header('Content-Type: application/json');
		echo json_encode($data);
	}

	public function editIsian()
	{
		$id = $this->input->post('idisian');

		$data =[
			'nama_isian' => $this->input->post('editnama'),
			'id_jabatan' => $this->input->post('editkategori'),
			'golongan' => $this->input->post('editgolongan'),
			'npwp' => $this->input->post('editnpwp'),
			'satuan' => $this->input->post('editsatuan'),
			'jml_brutto' => $this->input->post('editbrutto'),
			'pajak' => $this->input->post('editpajak'),
			'jml_netto' => $this->input->post('editnetto'),
		];

		$update = $this->Crud->update(array('id_isian'=>$id), 'isian_kegiatan', $data);
		if($update){
			echo 1;
		}else{
			echo 2;
		}
	}

	public function deleteIsian()
	{
		$id = $this->input->get('id');

		$delete = $this->Crud->delete(array('id_isian'=>$id), 'isian_kegiatan');
	}

	public function getAtk(){
		$id = $this->input->get('id');

		$data = [
			'atk' => $this->Crud->readAtkId($id),
		];
		
		return $this->load->view('tabelatk', $data);
	}

	public function tambahAtk(){
		$param = [
			'uraian_barang' => $this->input->post('nama_atk'),
			'tanggal_atk' => $this->input->post('tanggal_atk'),
			'satuan' => $this->input->post('satuan_atk'),
			'jumlah' => $this->input->post('jumlah_atk'),
			'jumlah_akhir' => $this->input->post('jumlahakhir_atk'),
			'id_kegiatan' => $this->input->post('id_kegiatan_atk'),
		];
		$this->Crud->create('isian_atk', $param);
	}

	public function getDataAtk(){
		$id = $this->input->get('id');

		$query = $this->Crud->readAtkById($id);
		foreach($query->result() as $result){
			$data = [
				'uraian_barang'=>$result->uraian_barang,
				'tanggal_atk'=>$result->tanggal_atk,
				'satuan'=>$result->satuan,
				'jumlah'=>$result->jumlah,
				'jumlah_akhir'=>$result->jumlah_akhir,
				'id_atk'=>$id,
			];
		}

		header('Content-Type: application/json');
		echo json_encode($data);
	}

	public function editAtk()
	{
		$id = $this->input->post('idatk');

		$data =[
			'uraian_barang' => $this->input->post('editnama_atk'),
			'tanggal_atk' => $this->input->post('edittanggal_atk'),
			'satuan' => $this->input->post('editsatuan_atk'),
			'jumlah' => $this->input->post('editjumlah_atk'),
			'jumlah_akhir' => $this->input->post('editjumlahakhir_atk')
		];

		$update = $this->Crud->update(array('id_atk'=>$id), 'isian_atk', $data);
		if($update){
			echo 1;
		}else{
			echo 2;
		}
	}

	public function deleteAtk()
	{
		$id = $this->input->get('id');

		$delete = $this->Crud->delete(array('id_atk'=>$id), 'isian_atk');
	}

	public function getPrlngkpn(){
		$id = $this->input->get('id');

		$data = [
			'prlngkpn' => $this->Crud->readPrlngkpnId($id),
		];
		
		return $this->load->view('tabelprlngkpn', $data);
	}

	public function tambahPrlngkpn(){
		$param = [
			'uraian_barang' => $this->input->post('nama_prlngkpn'),
			'tanggal_prlngkpn' => $this->input->post('tanggal_prlngkpn'),
			'satuan' => $this->input->post('satuan_prlngkpn'),
			'jumlah' => $this->input->post('jumlah_prlngkpn'),
			'jumlah_akhir' => $this->input->post('jumlahakhir_prlngkpn'),
			'id_kegiatan' => $this->input->post('id_kegiatan_prlngkpn'),
		];
		$this->Crud->create('isian_perlengkapan', $param);
	}

	public function getDataPrlngkpn(){
		$id = $this->input->get('id');

		$query = $this->Crud->readPrlngkpnById($id);
		foreach($query->result() as $result){
			$data = [
				'uraian_barang'=>$result->uraian_barang,
				'tanggal_prlngkpn'=>$result->tanggal_prlngkpn,
				'satuan'=>$result->satuan,
				'jumlah'=>$result->jumlah,
				'jumlah_akhir'=>$result->jumlah_akhir,
				'id_perlengkapan'=>$id,
			];
		}

		header('Content-Type: application/json');
		echo json_encode($data);
	}

	public function editPrlngkpn()
	{
		$id = $this->input->post('idprlngkpn');

		$data =[
			'uraian_barang' => $this->input->post('editnama_prlngkpn'),
			'tanggal_prlngkpn' => $this->input->post('edittanggal_prlngkpn'),
			'satuan' => $this->input->post('editsatuan_prlngkpn'),
			'jumlah' => $this->input->post('editjumlah_prlngkpn'),
			'jumlah_akhir' => $this->input->post('editjumlahakhir_prlngkpn')
		];

		$update = $this->Crud->update(array('id_perlengkapan'=>$id), 'isian_perlengkapan', $data);
		if($update){
			echo 1;
		}else{
			echo 2;
		}
	}

	public function deletePrlngkpn()
	{
		$id = $this->input->get('id');

		$delete = $this->Crud->delete(array('id_perlengkapan'=>$id), 'isian_perlengkapan');
	}

	public function getKonsumsi(){
		$id = $this->input->get('id');

		$data = [
			'konsumsi' => $this->Crud->readKonsumsiId($id),
		];
		
		return $this->load->view('tabelkonsumsi', $data);
	}

	public function tambahKonsumsi(){
		$param = [
			'uraian_barang' => $this->input->post('nama_konsumsi'),
			'tanggal_konsumsi' => $this->input->post('tanggal_konsumsi'),
			'satuan' => $this->input->post('satuan_konsumsi'),
			'jumlah' => $this->input->post('jumlah_konsumsi'),
			'jumlah_akhir' => $this->input->post('jumlahakhir_konsumsi'),
			'id_kegiatan' => $this->input->post('id_kegiatan_konsumsi'),
		];
		$this->Crud->create('isian_konsumsi', $param);
	}

	public function getDataKonsumsi(){
		$id = $this->input->get('id');

		$query = $this->Crud->readKonsumsiById($id);
		foreach($query->result() as $result){
			$data = [
				'uraian_barang'=>$result->uraian_barang,
				'tanggal_konsumsi'=>$result->tanggal_konsumsi,
				'satuan'=>$result->satuan,
				'jumlah'=>$result->jumlah,
				'jumlah_akhir'=>$result->jumlah_akhir,
				'id_konsumsi'=>$id,
			];
		}

		header('Content-Type: application/json');
		echo json_encode($data);
	}

	public function editKonsumsi()
	{
		$id = $this->input->post('idkonsumsi');

		$data =[
			'uraian_barang' => $this->input->post('editnama_konsumsi'),
			'tanggal_konsumsi' => $this->input->post('edittanggal_konsumsi'),
			'satuan' => $this->input->post('editsatuan_konsumsi'),
			'jumlah' => $this->input->post('editjumlah_konsumsi'),
			'jumlah_akhir' => $this->input->post('editjumlahakhir_konsumsi')
		];

		$update = $this->Crud->update(array('id_konsumsi'=>$id), 'isian_konsumsi', $data);
		if($update){
			echo 1;
		}else{
			echo 2;
		}
	}

	public function deleteKonsumsi()
	{
		$id = $this->input->get('id');

		$delete = $this->Crud->delete(array('id_konsumsi'=>$id), 'isian_konsumsi');
	}

	public function getTransportasi(){
		$id = $this->input->get('id');

		$data = [
			'transportasi' => $this->Crud->readTransportasiId($id),
		];
		
		return $this->load->view('tabeltransportasi', $data);
	}

	public function tambahTransportasi(){
		$param = [
			'nama_petugas' => $this->input->post('nama_transportasi'),
			'id_kota' => $this->input->post('kota'),
			'tanggal_pergi' => $this->input->post('tanggalawal_transportasi'),
			'tanggal_pulang' => $this->input->post('tanggalakhir_transportasi'),
			'satuan' => $this->input->post('satuan_transportasi'),
			'jumlah' => $this->input->post('jumlah_transportasi'),
			'jumlah_akhir' => $this->input->post('jumlahakhir_transportasi'),
			'id_kegiatan' => $this->input->post('id_kegiatan_transportasi'),
		];
		$this->Crud->create('isian_transportasi', $param);
	}

	public function getDataTransportasi(){
		$id = $this->input->get('id');

		$query = $this->Crud->readTransportasiById($id);
		foreach($query->result() as $result){
			$data = [
				'nama_transportasi'=>$result->nama_petugas,
				'kota'=>$result->id_kota,
				'namakota'=>$result->nama_kota,
				'tanggal_pergi'=>$result->tanggal_pergi,
				'tanggal_pulang'=>$result->tanggal_pulang,
				'satuan'=>$result->satuan,
				'jumlah'=>$result->jumlah,
				'jumlah_akhir'=>$result->jumlah_akhir,
				'id_transportasi'=>$id,
			];
		}

		header('Content-Type: application/json');
		echo json_encode($data);
	}

	public function editTransportasi()
	{
		$id = $this->input->post('idtransportasi');

		$data =[
			'nama_petugas' => $this->input->post('editnama_transportasi'),
			'id_kota' => $this->input->post('editkota'),
			'tanggal_pergi' => $this->input->post('edittanggalawal_transportasi'),
			'tanggal_pulang' => $this->input->post('edittanggalakhir_transportasi'),
			'satuan' => $this->input->post('editsatuan_transportasi'),
			'jumlah' => $this->input->post('editjumlah_transportasi'),
			'jumlah_akhir' => $this->input->post('editjumlahakhir_transportasi')
		];

		$update = $this->Crud->update(array('id_transportasi'=>$id), 'isian_transportasi', $data);
		if($update){
			echo 1;
		}else{
			echo 2;
		}
	}

	public function deleteTransportasi()
	{
		$id = $this->input->get('id');

		$delete = $this->Crud->delete(array('id_transportasi'=>$id), 'isian_transportasi');
	}

	public function addKerjasama(){
		$nama = $this->session->userdata("username");
		$iduser	= $this->session->userdata("iduser");
		$mitra   = $this->input->post('mitra');
		$kategori  = $this->input->post('kategori1');
		$subkategori1 = $this->input->post('asubkategori1');
		$subkategori2 = $this->input->post('asubkategori2');
		$subkategori3 = $this->input->post('asubkategori3');
		$subkategori4 = $this->input->post('asubkategori4');
		$judul  = $this->input->post('judul');
		$manfaat_m  = $this->input->post('manfaat_m');
		$manfaat_u  = $this->input->post('manfaat_u');
		$negara  = $this->input->post('negara');	
		$periode  = $this->input->post('periode');
		$tglusul  = $this->input->post('tglusul');
		$tglsls  = $this->input->post('tglsls');
		$bidang1  = $this->input->post('bidang1');
		$bidang2  = $this->input->post('bidang2');
		$bidang3  = $this->input->post('bidang3');
		$bidang4  = $this->input->post('bidang4');
		$bidang5  = $this->input->post('bidang5');
		$status = 2;
		$tahun = date("Y", strtotime("now"));
		$folder = gmdate("d-m-y-H-i-s", time()+3600*7);
		mkdir('./assets/file/'.$nama.'/berkas/'.$folder);
		//file1
		$fileName = gmdate("d-m-y-H-i-s", time()+3600*7)."-1.pdf";
		$config['upload_path'] = './assets/file/'.$nama.'/berkas/'.$folder;
		$config['allowed_types'] = 'pdf';
		$config['max_size'] = '2048000';
		$config['file_name'] = $fileName;
		$this->load->library('upload',$config);
		$this->upload->initialize($config);
		$this->upload->do_upload('file1');	
		//file2
		$fileName2 = gmdate("d-m-y-H-i-s", time()+3600*7)."-2.pdf";
		$config2['upload_path'] = './assets/file/'.$nama.'/berkas/'.$folder;
		$config2['allowed_types'] = 'pdf';
		$config2['max_size'] = '2048000';
		$config2['file_name'] = $fileName2;
		$this->load->library('upload',$config2);
		$this->upload->initialize($config2);
		$this->upload->do_upload('file2');
		//file3
		$fileName3 = gmdate("d-m-y-H-i-s", time()+3600*7)."-3.pdf";
		$config3['upload_path'] = './assets/file/'.$nama.'/berkas/'.$folder;
		$config3['allowed_types'] = 'pdf';
		$config3['max_size'] = '2048000';
		$config3['file_name'] = $fileName3;
		$this->load->library('upload',$config3);
		$this->upload->initialize($config3);
		$this->upload->do_upload('file3');
		//file4
		$fileName4 = gmdate("d-m-y-H-i-s", time()+3600*7)."-4.pdf";
		$config4['upload_path'] = './assets/file/'.$nama.'/berkas/'.$folder;
		$config4['allowed_types'] = 'pdf';
		$config4['max_size'] = '2048000';
		$config4['file_name'] = $fileName4;
		$this->load->library('upload',$config4);
		$this->upload->initialize($config4);
		$this->upload->do_upload('file4');

		if($kategori==1){
			$data   = array('id_user'=>$iduser, 'mitra'=>$mitra, 'id_kategori'=>$subkategori1, 'judul'=>$judul, 'manfaat_m'=>$manfaat_m,'manfaat_u'=>$manfaat_u, 'negara'=>$negara, 'periode'=>$periode, 'tgl_usul'=>$tglusul, 'tgl_lama'=>$tglsls, 'status'=>$status, 'tahun'=>$tahun, 'bidang1'=>$bidang1, 'bidang2'=>$bidang2, 'bidang3'=>$bidang3, 'bidang4'=>$bidang4, 'bidang5'=>$bidang5, 'file1'=>$fileName, 'file2'=>$fileName2, 'file3'=>$fileName3, 'file4'=>$fileName4, 'folder'=>$folder);
		}else if($kategori==2){
			$data   = array('id_user'=>$iduser, 'mitra'=>$mitra, 'id_kategori'=>$subkategori2, 'judul'=>$judul, 'manfaat_m'=>$manfaat_m,'manfaat_u'=>$manfaat_u, 'negara'=>$negara, 'periode'=>$periode, 'tgl_usul'=>$tglusul, 'tgl_lama'=>$tglsls, 'status'=>$status, 'tahun'=>$tahun, 'bidang1'=>$bidang1, 'bidang2'=>$bidang2, 'bidang3'=>$bidang3, 'bidang4'=>$bidang4, 'bidang5'=>$bidang5, 'file1'=>$fileName, 'file2'=>$fileName2, 'file3'=>$fileName3, 'file4'=>$fileName4, 'folder'=>$folder);
		}else if($kategori==3){
			$data   = array('id_user'=>$iduser, 'mitra'=>$mitra, 'id_kategori'=>$subkategori3, 'judul'=>$judul, 'manfaat_m'=>$manfaat_m,'manfaat_u'=>$manfaat_u, 'negara'=>$negara, 'periode'=>$periode, 'tgl_usul'=>$tglusul, 'tgl_lama'=>$tglsls, 'status'=>$status, 'tahun'=>$tahun, 'bidang1'=>$bidang1, 'bidang2'=>$bidang2, 'bidang3'=>$bidang3, 'bidang4'=>$bidang4, 'bidang5'=>$bidang5, 'file1'=>$fileName, 'file2'=>$fileName2, 'file3'=>$fileName3, 'file4'=>$fileName4, 'folder'=>$folder);
		}else if($kategori==4){
			$data   = array('id_user'=>$iduser, 'mitra'=>$mitra, 'id_kategori'=>$subkategori4, 'judul'=>$judul, 'manfaat_m'=>$manfaat_m,'manfaat_u'=>$manfaat_u, 'negara'=>$negara, 'periode'=>$periode, 'tgl_usul'=>$tglusul, 'tgl_lama'=>$tglsls, 'status'=>$status, 'tahun'=>$tahun, 'bidang1'=>$bidang1, 'bidang2'=>$bidang2, 'bidang3'=>$bidang3, 'bidang4'=>$bidang4, 'bidang5'=>$bidang5, 'file1'=>$fileName, 'file2'=>$fileName2, 'file3'=>$fileName3, 'file4'=>$fileName4, 'folder'=>$folder);
		}
		
		$insert = $this->Crud->create('tb_mou', $data);
		redirect(site_url('DashUser'), 'refresh');

	}

	public function lihatFile($usr,$bid,$fol, $id, $ext){
       $file_location = 'assets/file/'.$usr.'/'.$bid.'/'.$fol;
       switch($ext){
           case 'pdf':
             $file_location = 'assets/file/'.$usr.'/'.$bid.'/'.$fol; // store as constant maybe inside index.php - PDF = 'uploads/pdf/';

             //must have PDF viewer installed in browser !
          $this->output
           ->set_content_type('application/pdf')
           ->set_output(file_get_contents($file_location . '/' . $id));

           break;
           //jpg gif etc here...
       }

    }

	public function cekFile1(){
		$path  = $this->input->post('file1');
		$file = pathinfo($path, PATHINFO_EXTENSION);
		if($file == 'pdf'){
			echo 'true';
		}else{
			echo 'false';
		}
	}

	public function cekFile2(){
		$path  = $this->input->post('file2');
		$file = pathinfo($path, PATHINFO_EXTENSION);
		if($file == 'pdf'){
			echo 'true';
		}else{
			echo 'false';
		}
	}

	public function cekFile3(){
		$path  = $this->input->post('file3');
		$file = pathinfo($path, PATHINFO_EXTENSION);
		if($file == 'pdf'){
			echo 'true';
		}else{
			echo 'false';
		}
	}

	public function cekFile4(){
		$path  = $this->input->post('file4');
		$file = pathinfo($path, PATHINFO_EXTENSION);
		if($file == 'pdf'){
			echo 'true';
		}else{
			echo 'false';
		}
	}

	public function cekData($table, $field, $data){
		$match = $this->Crud->read($table, array($field=>$data), null, null);
		if($match->num_rows() > 0){
			$report = 2;
		}else{
			$report = 1;
		}
		echo $report;
	}

	public function detStat($id){
        $data['detstat'] = $this->Crud->readProses($id);
        $this->load->view('tabel_detail',$data);
    }

	public function update($id){
		$data =[
			'nama'   => $this->input->post('editnama'),
			'id_katkeg'  => $this->input->post('editkegiatan'),
			'tanggal' => $this->input->post('edittanggal'),
			'jam' => $this->input->post('editjam'),
			'lokasi' => $this->input->post('editlokasi'),
			'dasar_hukum' => $this->input->post('editdashuk')
		];

		$update = $this->Crud->update(array('id_kegiatan'=>$id), 'kegiatan', $data);
		if($update){
			echo 1;
		}else{
			echo 2;
		}
	}

	public function delete($id)
    {
     	$delete = $this->Crud->delete(array('id_kegiatan'=>$id), 'kegiatan');
    }

}