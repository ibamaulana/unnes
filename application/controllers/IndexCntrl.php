<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class IndexCntrl extends MY_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->helper('form');
		$this->load->helper('file');
		$this->load->model('Crud');
	}

	public function index(){
		$profil = $this->Crud->read('tb_profil', null, null, null)->result();
		foreach($profil as $data){
			$title = $data->namaweb;
			$slogan = $data->sloganweb;
			$deskripsi = $data->deskripsiweb;
		}
		$data=[
			'title' => $title,
			'slogan' => $slogan,
			'deskripsiweb' => $deskripsi,
			'date' => date("l, d-m-Y", strtotime("now")),
			'data' => $this->Crud->read('tb_user', null, null, null),
			'kebun' => $this->Crud->kebunLengkap(),
			//'binaan' => $this->Crud->readDesaHome(1),
			//'mitra' => $this->Crud->readDesaHome(2),
			//'kegiatan' => $this->Crud->readKegiatanAllHome(),
			//'agenda' => $this->Crud->readAgendaHome(),
			//'berita' => $this->Crud->readBeritaHome(),
			//'jml_binaan' => $this->Crud->readDesa(1)->num_rows(), 
			//'jml_mitra' => $this->Crud->readDesa(2)->num_rows(), 
			//'jml_kegiatan' => $this->Crud->readKegiatan()->num_rows(), 
			//'jml_agenda' => $this->Crud->readAgenda()->num_rows(), 
		];
		$this->load->view('home', $data);
	}
	
	public function detail_kebun(){
		$id = $_GET['id'];
		$query = $this->Crud->readKebunId($id);
		$query2 = $this->Crud->readGambarKebun($id);
		$data = [
			'title' => 'Dinas Pertanian Kota Semarang',
			'nama_kebun' => 'Kebun Cepoko',
			'kebun' => $query,
			'gambar_kebun' => $query2,
		];
		$this->load->view('detail_kebun',$data);
	}
	
}