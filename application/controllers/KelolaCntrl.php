<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class KelolaCntrl extends MY_Controller {
	public function __construct()
    {
		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->helper('file');
		$this->load->model('Crud');
	}

	//kategori ==============================================================================================
		public function kategori()
		{
			$id=$this->session->userdata('iduser');
			$data =[
				'title' => 'DISPERTAN | Kelola Kategori',
				'date' => date("l, d-m-Y", strtotime("now")),
				'profil' => $this->Crud->readProfil()
			];
			$this->load->view('admin/kelolakategori', $data);
		}

		public function getKategori()
		{
			$data = [
				'kategori' => $this->Crud->readKategori(),
			];

			$this->load->view('admin/tabel-kategori', $data);
		}

		public function addKategori()
		{
			$param = [
				'nama_kategori' => $this->input->post('nama_kategori'),
				'deskripsi_kategori' => $this->input->post('deskripsi_kategori')
			];

			$this->Crud->create('tb_kategori', $param);
		}

		public function editKategori()
		{
			$id = $this->input->post('id_kategori');

			$data =[
				'nama_kategori' => $this->input->post('editnama_kategori'),
				'deskripsi_kategori' => $this->input->post('editdeskripsi_kategori'),
			];

			$update = $this->Crud->update(array('id_kategori'=>$id), 'tb_kategori', $data);
		}

		public function hapusKategori()
		{
			$id = $this->input->get('id');

			$update = $this->Crud->delete(array('id_kategori'=>$id), 'tb_kategori');
		}

		public function getDataKategori(){
			$id = $this->input->get('id');

			$desa = $this->Crud->readKategoriId($id);

			foreach($desa->result() as $result){
				$data = [
					'nama_kategori'=>$result->nama_kategori,
					'deskripsi_kategori'=>$result->deskripsi_kategori,
					'id_kategori'=>$result->id_kategori
				];
			}

			header('Content-Type: application/json');
			echo json_encode($data);
		}
	//end kategori ==========================================================================================
	
	//produk ================================================================================================
		public function produk()
		{
			$id=$this->session->userdata('iduser');
			$data =[
				'title' => 'DISPERTAN | Kelola Produk',
				'date' => date("l, d-m-Y", strtotime("now")),
				'profil' => $this->Crud->readProfil(),
				'kategori' => $this->Crud->readKategori()
				//'desa' => $this->Crud->readDesaAll()
			];
			$this->load->view('admin/kelolaproduk', $data);
		}

		public function getProduk()
		{
			$status = $this->input->get('status');

			$data = [
				'produk' => $this->Crud->readProduk()
			];

			return $this->load->view('admin/tabel-produk', $data);
		}

		public function addProduk()
		{
			$param = [
				'nama_produk' => $this->input->post('nama_produk'),
				'deskripsi_produk' => $this->input->post('deskripsi_produk'),
				'berat' => $this->input->post('berat_produk'),
				'penjual' => $this->input->post('penjual_produk'),
				'kontak' => $this->input->post('kontak'),
				'id_kategori' => $this->input->post('kategori')
			];

			$this->Crud->create('tb_produk', $param);
		}

		public function editProduk()
		{
			$id = $this->input->post('id_produk');

			$data =[
				'nama_produk' => $this->input->post('editnama_produk'),
				'deskripsi_produk' => $this->input->post('editdeskripsi_produk'),
				'berat' => $this->input->post('editberat_produk'),
				'penjual' => $this->input->post('editpenjual_produk'),
				'kontak' => $this->input->post('editkontak'),
				'id_kategori' => $this->input->post('editkategori')
			];

			$update = $this->Crud->update(array('id_produk'=>$id), 'tb_produk', $data);
		}

		public function hapusProduk()
		{
			$id = $this->input->get('id');

			$delete = $this->Crud->delete(array('id_produk'=>$id), 'tb_produk');
			$delete = $this->Crud->delete(array('id_produk'=>$id), 'tb_gambar_produk');
		}

		public function getDataProduk(){
			$id = $this->input->get('id');

			$desa = $this->Crud->readProdukId($id);

			foreach($desa->result() as $result){
				$data = [
					'id_produk'=>$result->id_produk,
					'nama_produk'=>$result->nama_produk,
					'deskripsi'=>$result->deskripsi_produk,
					'berat'=>$result->berat,
					'penjual'=>$result->penjual,
					'kontak'=>$result->kontak,
					'id_kategori'=>$result->id_kategori,
					'nama_kategori'=>$result->nama_kategori
				];
			}

			header('Content-Type: application/json');
			echo json_encode($data);
		}
		
		public function getGambarProduk()
		{
			$id = $this->input->get('id');

			$data = [
				'gambar' => $this->Crud->readGambarProduk($id),
			];

			return $this->load->view('admin/gambar-produk', $data);
		}

		public function addGambarProduk()
		{
			$id_produk = $this->input->post('id_produk');

			$this->upload_files('id_produk',$id_produk,'tb_gambar_produk','assets/produk/','produk', $_FILES['fileberkas']);
		}

		public function hapusGambarProduk()
		{
			$id = $this->input->get('id');

			$delete = $this->Crud->delete(array('id_gambar_produk'=>$id), 'tb_gambar_produk');
		}
	//end produk ==========================================================================================
		
	//kebun ===============================================================================================
		public function kebun()
		{
			$id = $this->session->userdata('iduser');
			$data =[
				'title' => 'DISPERTAN | Kelola Kebun',
				'date' => date("l, d-m-Y", strtotime("now"))
			];
			
			$lokasi_kebun = $this->input->post('lokasi');
			
			if(isset($lokasi_kebun)){
				$id_kebun = $this->input->post('id_kebun');
				$data_update =[
					'lokasi' => $this->input->post('lokasi'),
					'lat' => $this->input->post('lat'),
					'lng' => $this->input->post('lng')
				];

				$update = $this->Crud->update(array('id_kebun'=>$id_kebun), 'tb_kebun', $data_update);
			}
	
			$this->load->view('admin/kelolakebun', $data);
		}

		public function getKebun()
		{
			$data = [
				'kebun' => $this->Crud->readKebun(),
			];

			return $this->load->view('admin/tabel-kebun', $data);
		}

		public function addKebun()
		{
			$param = [
				'nama_kebun' => $this->input->post('nama_kebun'),
				'deskripsi_kebun' => $this->input->post('deskripsi_kebun'),
				'pemilik' => $this->input->post('pemilik'),
				//'lokasi' => $this->input->post('lokasi'),
			];

			$this->Crud->create('tb_kebun', $param);
		}

		public function editKebun()
		{
			$id = $this->input->post('id_kebun');

			$data =[
				'nama_kebun' => $this->input->post('editnama_kebun'),
				'deskripsi_kebun' => $this->input->post('editdeskripsi_kebun'),
				'pemilik' => $this->input->post('editpemilik'),
				//'lokasi' => $this->input->post('editlokasi'),
			];

			$update = $this->Crud->update(array('id_kebun'=>$id), 'tb_kebun', $data);
		}
	
		public function lokasiKebun(){
			$id = $this->input->get('id');
			
			$kebun = $this->Crud->readKebunId($id);

			foreach($kebun->result() as $result){
				$data = [
					'title' => 'DISPERTAN | Lokasi Kebun',
					'date' => date("l, d-m-Y", strtotime("now")),
					'id_kebun'=>$result->id_kebun,
					'nama_kebun'=>$result->nama_kebun,
					'deskripsi_kebun'=>$result->deskripsi_kebun,
					'lokasi'=>$result->lokasi,
					'lat'=>$result->lat,
					'lng'=>$result->lng,
				];
			}
			
			$this->load->view('admin/lokasi_kebun', $data);
		}
		
		public function hapusKebun()
		{
			$id = $this->input->get('id');

			$delete = $this->Crud->delete(array('id_kebun'=>$id), 'tb_kebun');
		}

		public function getDataKebun(){
			$id = $this->input->get('id');

			$kebun = $this->Crud->readKebunId($id);

			foreach($kebun->result() as $result){
				$data = [
					'id_kebun'=>$result->id_kebun,
					'nama_kebun'=>$result->nama_kebun,
					'deskripsi_kebun'=>$result->deskripsi_kebun,
					'lokasi'=>$result->lokasi,
				];
			}

			header('Content-Type: application/json');
			echo json_encode($data);
		}
		
		public function getGambarKebun()
		{
			$id = $this->input->get('id');

			$data = [
				'gambar' => $this->Crud->readGambarKebun($id),
			];

			return $this->load->view('admin/gambar-kebun', $data);
		}

		public function addGambarKebun()
		{
			$id_kebun = $this->input->post('id_kebun');

			$this->upload_files('id_kebun',$id_kebun,'tb_gambar_kebun','assets/kebun/','kebun', $_FILES['fileberkas']);
		}

		public function hapusGambarKebun()
		{
			$id = $this->input->get('id');

			$delete = $this->Crud->delete(array('id_gambar_kebun'=>$id), 'tb_gambar_kebun');
		}
	//end kebun ===========================================================================================
	
	//upload file =========================================================================================
		private function upload_files($namaid, $id, $table, $path, $title, $files)
		{
			$config = array(
				'upload_path'   => './'.$path,
				'allowed_types' => 'pdf|gif|jpeg|png|jpg|docx|doc|images',
				'overwrite'     => 1,    
				'max_size'		=> '5048000'               
			);

			$this->load->library('upload', $config);

			$images = array();
			$i=0;
			foreach ($files['name'] as $key => $image) {
				$i++;
				$_FILES['images[]']['name']= $files['name'][$key];
				$_FILES['images[]']['type']= $files['type'][$key];
				$_FILES['images[]']['tmp_name']= $files['tmp_name'][$key];
				$_FILES['images[]']['error']= $files['error'][$key];
				$_FILES['images[]']['size']= $files['size'][$key];

				$fileName = gmdate("d-m-y-H-i-s", time()+3600*7).'-'.$title;
				$name = $title.'-'.gmdate("dmyHis", time()+3600*7).'-'.$i;

				$images[] = $name;

				$config['file_name'] = $name;

				$ext = pathinfo($files['name'][$key], PATHINFO_EXTENSION);

				$this->upload->initialize($config);

				if ($this->upload->do_upload('images[]')) {
					$this->upload->data();
				} else {
					return false;
				}
				$data = [
					$namaid => $id, 
					'path' => $path.$name.'.'.$ext,
					//'status' => 1,
				];

				$this->Crud->create($table,$data);
			}

			return true;
		}
	// end upload file =======================================================================================

}